<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment', function(Blueprint $table) {
			$table->increments('id');
			$table->string('req_id');
			$table->string('txnid');
			$table->string('mihpayid');
			$table->string('mode');
			$table->string('status');
			$table->string('amount');
			$table->string('unmappedstatus');
			$table->string('payment_msg');
			$table->string('pg_type');
			$table->string('error_code');
			$table->string('error_msg');
			$table->string('payuMoneyId');
			$table->string('bank_ref_num');
			$table->string('bankcode');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment');
	}

}
