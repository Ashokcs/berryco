<?php

function asset_url() {
    return URL::to('/');
}

function get_brewer_heading($id){
	$brewer = Brewer::find($id);
	$brewer_heading = $brewer->heading;

	return $brewer_heading;
}

function get_name($id)
{
	$user = User::find($id);

	return $user->name;

}
function get_email()
{
	$id = Auth::user()->id();
	$user = User::find($id);

	return $user->email;

}


function get_number_format($amount,$num)
{
	$price = number_format($amount,$num);
	return $price;
}

function get_subtotal(){
	$ids = Session::get("ids");
	
			$total = 0;
	if($ids)
		{
			//seperate ids
			$exp = explode(',',$ids);
			foreach ($exp as $key ) {
				$sep = explode('.', $key);
				$get_item = Item::where('id',$sep[0])->first();

				$total =$total + ($sep[1] * $get_item->price);
				//     total + ( quantity * price of item )

			}

		}


	return $total;
}

function save_request(){
	
	$user_id = Auth::user()->id();
		//save ever orders
		$name = Input::get('firstname');
		$phone = Input::get('phone');
		$address_1 = Input::get('address_1');
		$address_2 = Input::get('address_2');
		$state = Input::get('state');
		$city = Input::get('city');
		$pincode = Input::get('zipcode');
		$ids = Session::get('ids');

		if($ids){

			$total = 0;
			$exp = explode(',',$ids);
			foreach ($exp as $key ) {
				$sep = explode('.', $key);
				$get_item = Item::where('id',$sep[0])->first();

				$total =$total + ($sep[1] * $get_item->price);

			}
			
			$user = User::where('id',$user_id)->first();
			$email = $user->email;
		$request = new Requests;
		$request->name = $name;
		$request->user_id = $user_id;
		$request->phone = $phone;
		$request->email = $email;
		$request->address = $address_1.$address_2;
		$request->state = $state;
		$request->city = $city;
		$request->pincode = $pincode;
		$request->items = $ids;
		$request->is_paid = 0;

		$request->total = $total;

		$request->save();
		Session::forget('ids');

	}

}

function get_pay_status($val){
	if($val == 1){
		$status = '<span class="label label-success" >Paid</span>';
	}else{
		$status = '<span class="label label-danger" >No</span>';
	}

	return $status;
}