<?php

class WebController extends Controller {


	public function __construct() {
        $this->beforeFilter(function () {
            if (!Auth::user()->check()) {
                $url = URL::current();
                $routeName = Route::currentRouteName();
                Log::info('current route =' . print_r(Route::currentRouteName(), true));

                if ($routeName != "WebLogin" && $routeName != 'Wensiginup') {
                    Session::put('pre_admin_login_url', $url);
                    if(Input::has('array1')){
                    	$ids = Input::get('array1');
                    	Session::put('ids',$ids);
                    }
                }
                return Redirect::to('/login');
            }
        }, array('except' => array(
        	'index',
        	'login',
        	'signup',
        	'about',
        	'service',
        	'blog',
        	'faq',
        	'weare',
        	'qur',
        	'equipment',
        	'save_signup',
        	'put_session',
        	'add_cart','buy_add_cart' )));
    }



	public function index()
	{
		return View::make('website.index');
	}

	public function login()
	{
		return View::make('website.login');
	}
	public function signup()
	{
		return View::make('website.signup');
	}

	public function about()
	{
		return View::make('website.about');
	}

	public function service()
	{
		return View::make('website.service');
	}

	public function blog()
	{
		return View::make('website.blog');
	}

	public function faq()
	{
		return View::make('website.faq');
	}

	public function weare()
	{
		return View::make('website.weare');
	}

	public function qur()
	{
		return View::make('website.qur');
	}

	public function logout()
	{
		Auth::user()->logout();
		return Redirect::back();
	}

	public function equipment()
	{
		$ids = Session::get('ids');
		$rp = explode(',', $ids);

		if($ids){
		$count = count($rp);
		}else{
		$count = 0;
		}
		
		$brewers = Brewer::all();
		$items = Item::all();

		

		return View::make('website.equipment')->with('brewers',$brewers)->with('cart',$ids)->with('items',$items)->with('count',$count);
	}

	public function save_signup()
	{
		$social_unique_id = Input::get('social_unique_id');
		$name = Input::get('name');
		$login_by = Input::get('login_by');
		$phone = Input::get('phone');
		$email = Input::get('email');
		$address = Input::get('address');
		$pincode = Input::get('pincode');

		$user = new User;
		$user->social_unique_id = $social_unique_id;
		$user->password = Hash::make($social_unique_id);
		$user->name = $name;
		$user->login_by = $login_by;
		$user->phone = $phone;
		$user->email = $email;
		$user->address = $address;
		$user->pincode = $pincode;
		$user->save();

		if (Session::has('pre_admin_login_url')) {
                    $url = Session::get('pre_admin_login_url');
                    Session::forget('pre_admin_login_url');
                    return Redirect::to($url);
        }else{
					return Redirect::to('/');
		}
	}

	public function put_session()
	{
		$ids = Input::get('array1');
		Session::put('ids',$ids);
		return Redirect::to('/checkout');
	}

	public function checkout()
	{

		$ids = Session::get('ids');
		$rp = explode(',', $ids);

		$count = count($rp);
		if($ids)
		{

			//seperate ids
			$products = array();
			$exp = explode(',',$ids);
			foreach ($exp as $key ) {
				$item = array();
				$sep = explode('.', $key);
				$get_item = Item::where('id',$sep[0])->first();
				$item['id'] = $get_item->id;
				$item['name'] = $get_item->name;
				$item['image'] = $get_item->image;
				$item['price'] = $get_item->price;
				$item['quantity'] = $sep[1];
				array_push($products,$item);
			}

			// $items_get = "SELECT * from items where id IN($ids)";
			// $items = DB::select(DB::raw($items_get));

			// $total = 0;
			// foreach ($items as $key) {
			// 	$total = $total+$key->price;
			// }
			$id = Auth::user()->id();
			$user = User::find($id);

			return View::make('website.checkout')->with('items',$products)->with('user',$user)->with('total',50);
		}else{
			return Redirect::to('/equipment');
		}

		
	}

	public function remove_item()
	{
		$item = Request::segment('2');
		
		$ids = Session::get('ids');

		$rp = explode(',', $ids);

		$count = count($rp);
		if($count > 0){

		$k =array();
		foreach($rp as $r){
			$seprate = explode('.',$r);
			if($seprate[0] == $item){
				if($seprate[1] > 1) {
						$item_id = $seprate[0];
						$quantity = $seprate[1] - 1;
						$product = $item_id.".".$quantity;
						array_push($k,$product);
				}	
			}else{
				array_push($k,$r);
			}
		}
			$i = implode(',', $k);
		Session::put('ids',$i);
		return Redirect::to('/checkout');
	}else{
		
		Session::forget('ids');
		return Redirect::to('/equipment');
	}

	}

	public function add_cart()
	{
		Session::put('success_msg','1');

		$item = Input::get('id');

		$ids = Session::get('ids');

		if($ids)
		{
		$rp = explode(',', $ids);

		$count = count($rp);
		if($count > 0){
			$k =array();
			$set = 0;
			foreach($rp as $r){
					$seprate = explode('.',$r);
				if($seprate[0] == $item){
					$set = 1;
					$item_id = $seprate[0];
					$quantity = $seprate[1] + 1;
					$product = $item_id.".".$quantity;
					array_push($k,$product);
				}else{
					// $product = $r.".".'1';
					array_push($k,$r);
				}
			}
				if(!$set){
					$product = $item.".".'1';
					array_push($k,$product);
				}
				$i = implode(',', $k);
				Session::put('ids',$i);


		}else{
			$product = $item.".".'1';
			Session::put('ids',$product);
		}
	}else{
			$product = $item.".".'1';
			Session::put('ids',$product);
	}



		// return Redirect::to('/equipment');
	}

	public function full_remove_item()
	{
		$item = Request::segment('2');
		
		$ids = Session::get('ids');

		$rp = explode(',', $ids);

		$count = count($rp);
		if($count > 0){

		$k =array();
		foreach($rp as $r){
			$seprate = explode('.',$r);
			if($seprate[0] == $item){
				// skip the item	
			}else{
				array_push($k,$r);
			}
		}
			$i = implode(',', $k);
		Session::put('ids',$i);
		return Redirect::to('/checkout');
	}else{
		
		Session::forget('ids');
		return Redirect::to('/equipment');
	}

	}



	public function final_checkout()
	{

		$user_id = Auth::user()->id();
		//save ever orders
		$name = Input::get('name');
		$phone = Input::get('phone');
		$address_1 = Input::get('address_1');
		$address_2 = Input::get('address_2');
		$state = Input::get('state');
		$city = Input::get('city');
		$pincode = Input::get('pincode');
		$ids = Session::get('ids');

		
		$check = Requests::where('user_id',$user_id)->where('is_paid',0)->count();

		if(!$check){

		if($ids){

			$total = 0;
			$exp = explode(',',$ids);
			foreach ($exp as $key ) {
				$sep = explode('.', $key);
				$get_item = Item::where('id',$sep[0])->first();

				$total =$total + ($sep[1] * $get_item->price);
				//     total + ( quantity * price of item )

			}
			
			$user = User::where('id',$user_id)->first();
			$email = $user->email;
		$request = new Requests;
		$request->name = $name;
		$request->user_id = $user_id;
		$request->phone = $phone;
		$request->email = $email;
		$request->address = $address_1.$address_2;
		$request->state = $state;
		$request->city = $city;
		$request->pincode = $pincode;
		$request->items = $ids;
		$request->is_paid = 0;

		$request->total = $total;

		$request->save();
		Session::forget('ids');
		
		}
	}else{
		$request = Requests::where('user_id',$user_id)->where('is_paid',0)->first();

		$items_get = "SELECT * from items where id IN($request->items)";
			$items = DB::select(DB::raw($items_get));

	}
		return View::make('website.complete_payment')->with('request',$request); 


	}

public function buy_add_cart()
	{
		$item = Request::segment('2');

		$ids = Session::get('ids');

		if($ids)
		{
		$rp = explode(',', $ids);

		$count = count($rp);
		if($count > 0){
			$k =array();
			$set = 0;
			foreach($rp as $r){
					$seprate = explode('.',$r);
				if($seprate[0] == $item){
					$set = 1;
					$item_id = $seprate[0];
					$quantity = $seprate[1] + 1;
					$product = $item_id.".".$quantity;
					array_push($k,$product);
				}else{
					// $product = $r.".".'1';
					array_push($k,$r);
				}
			}
				if(!$set){
					$product = $item.".".'1';
					array_push($k,$product);
				}
				$i = implode(',', $k);
				Session::put('ids',$i);


		}else{
			$product = $item.".".'1';
			Session::put('ids',$product);
		}
	}else{
			$product = $item.".".'1';
			Session::put('ids',$product);
	}




		return Redirect::to('/checkout');
	}



	

}
