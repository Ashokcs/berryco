<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function loginWithFacebook() {

    // get data from input
    $code = Input::get( 'code' );

    // get fb service
    $fb = OAuth::consumer( 'Facebook' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

        // This was a callback request from facebook, get the token
        $token = $fb->requestAccessToken( $code );

        // Send a request with it
        $result = json_decode( $fb->request( '/me?fields=id,name,email'), true );

        // $message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
        // echo $message. "<br/>";

        $user = User::where('social_unique_id',$result['id'])->first();
        if($user)
        {
            if(Auth::user()->attempt(array('social_unique_id'=>$result['id'],'password'=>$result['id']))){
                if (Session::has('pre_admin_login_url')) {
                    $url = Session::get('pre_admin_login_url');
                    Session::forget('pre_admin_login_url');
                    return Redirect::to($url);
                }else{
                    return Redirect::route('WebIndex'); 
                }
            }else{
                return Redirect::to('/login');
            }

        }else{
            return View::make('website/signup_finish')->with('result',$result)->with('login_by','facebook');
        }
        

    }
    // if not ask for permission first
    else {
        // get fb authorization
        $url = $fb->getAuthorizationUri();

        // return to facebook login url
         return Redirect::to( (string)$url );
    }

}

public function facebook_signup() {

    // get data from input
    $code = Input::get( 'code' );

    // get fb service
    $fb = OAuth::consumer( 'Facebook' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

        // This was a callback request from facebook, get the token
        $token = $fb->requestAccessToken( $code );

        // Send a request with it
        $result = json_decode( $fb->request( '/me?fields=id,name,email'), true );

        // $message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
        // echo $message. "<br/>";
        // dd($result);

        $check = User::where('social_unique_id',$result['id'])->first();
        if(!$check)
        {
            return View::make('website/signup_finish')->with('result',$result)->with('login_by','facebook');

        }else{

            if(Auth::user()->attempt(array('social_unique_id'=>$result['id'],'password'=>$result['id']))){
                if (Session::has('pre_admin_login_url')) {
                    $url = Session::get('pre_admin_login_url');
                    Session::forget('pre_admin_login_url');
                    return Redirect::to($url);
                }else{
                return Redirect::route('WebIndex'); 
                }
            }else{
                return Redirect::to('/login');
            }
        }

    }
    // if not ask for permission first
    else {
        // get fb authorization
        $url = $fb->getAuthorizationUri();

        // return to facebook login url
         return Redirect::to( (string)$url );
    }

}





public function loginWithGoogle() {

    // get data from input
    $code = Input::get( 'code' );

    // get google service
    $googleService = OAuth::consumer( 'Google' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

        // This was a callback request from google, get the token
        $token = $googleService->requestAccessToken( $code );

        // Send a request with it
        $result = json_decode( $googleService->request( 'https://www.googleapis.com/oauth2/v1/userinfo' ), true );

        $message = 'Your unique Google user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
        
        $user = User::where('social_unique_id',$result['id'])->first();
        if($user)
        {
            if(Auth::user()->attempt(array('social_unique_id'=>$result['id'],'password'=>$result['id']))){
                if (Session::has('pre_admin_login_url')) {
                    $url = Session::get('pre_admin_login_url');
                    Session::forget('pre_admin_login_url');
                    return Redirect::to($url);
                }else{
                return Redirect::route('WebIndex'); 
                }
            }else{
                return Redirect::to('/login');
            }

        }else{
            return View::make('website/signup_finish')->with('result',$result)->with('login_by','google');
        }
        

    }
    // if not ask for permission first
    else {
        // get googleService authorization
        $url = $googleService->getAuthorizationUri();

        // return to google login url
        return Redirect::to( (string)$url );
    }
}


    public function google_signup()
    {

         // get data from input
    $code = Input::get( 'code' );

    // get google service
    $googleService = OAuth::consumer( 'Google' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

        // This was a callback request from google, get the token
        $token = $googleService->requestAccessToken( $code );

        // Send a request with it
        $result = json_decode( $googleService->request( 'https://www.googleapis.com/oauth2/v1/userinfo' ), true );

        $message = 'Your unique Google user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
        
          $check = User::where('social_unique_id',$result['id'])->first();
        if(!$check)
        {
            return View::make('website/signup_finish')->with('result',$result)->with('login_by','google');

        }else{

            if(Auth::user()->attempt(array('social_unique_id'=>$result['id'],'password'=>$result['id']))){
                if (Session::has('pre_admin_login_url')) {
                    $url = Session::get('pre_admin_login_url');
                    Session::forget('pre_admin_login_url');
                    return Redirect::to($url);
                }else{
                return Redirect::route('WebIndex'); 
                }
            }else{
                return Redirect::to('/login');
            }
        }
    }
    // if not ask for permission first
    else {
        // get googleService authorization
        $url = $googleService->getAuthorizationUri();

        // return to google login url
        return Redirect::to( (string)$url );
    }
    }



}
