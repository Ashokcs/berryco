<?php

class PaymentController extends Controller {


	public function payment(){


	$user_id = Auth::user()->id();
		
	
	 $posted = array();
	 $posted['email'] = Input::get('email');		
	 // $posted['productinfo'] = "Product.".$k.".info";		
	 $posted['surl'] = Input::get('surl');		
	 $posted['furl'] = Input::get('furl');		
	 $posted['firstname'] = Input::get('firstname');		
	 $posted['address1'] = Input::get('address1');		
	 $posted['address2'] = Input::get('address2');		
	 $posted['state'] = Input::get('state');		
	 $posted['city'] = Input::get('city');		
	 $posted['zipcode'] = Input::get('zipcode');		
	 $posted['phone'] = Input::get('phone');		
	 $posted['amount'] = Input::get('amount');
	 $posted['service_provider'] = 'payu_paisa';
	 

	$ids = Session::get('ids');

		if($ids){

			$total = 0;
			$exp = explode(',',$ids);
			foreach ($exp as $key ) {
				$sep = explode('.', $key);
				$get_item = Item::where('id',$sep[0])->first();

				$total =$total + ($sep[1] * $get_item->price);

			}
			
			$user = User::where('id',$user_id)->first();
			$email = $user->email;
		$request = new Requests;
		$request->name = $posted['firstname'];
		$request->user_id = $user_id;
		$request->phone = $posted['phone'];
		$request->email = $posted['email'];
		$request->address = $posted['address1'].$posted['address2'];
		$request->state = $posted['state'];
		$request->city = $posted['city'];
		$request->pincode = $posted['zipcode'];
		// $request->txnid = $posted['txnid'];
		$request->items = $ids;
		$request->is_paid = 0;

		$request->total = $total;

		$request->save();
		Session::forget('ids');
		$req_id = $request->id;
	 	$posted['productinfo'] = (string)"Product.".$req_id.".info";

	}

	 // Merchant key here as provided by Payu
	 $posted['key'] = 'zev7C080'; //live
	 // $posted['key'] = 'rjQUPktU'; //test


// generate transact id - start //	 
	 if(Input::get('txnid')){
	 	$posted['txnid'] = Input::get('txnid');
	 }else{
	 	$posted['txnid'] = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	 	
	 }
// transact is end //
	 	if($posted['txnid']){
		 	$request->txnid = $posted['txnid'];
		 	$request->save();
	 	}
	 
//  generate hash - start //	 	
	 
// $PAYU_BASE_URL = "https://test.payu.in"; //test
// $SALT = "e5iIg1jwi8";  //test
$PAYU_BASE_URL = "https://secure.payu.in";  //live
$SALT = "nszkdOvncg"; //live
$hash = '';

 if(Input::get('hash') && !empty($posted['hash'])){
	 	$posted['hash'] = Input::get('hash');
	 }else{
	 	$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
	 	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;
    $hash = strtolower(hash('sha512', $hash_string));
	$posted['hash'] = $hash;
	 	
	}
//  generate hash - end //

	$action = $PAYU_BASE_URL . '/_payment';


	if(!empty($posted['hash'])){

	 return View::make('website.complete_payment')->with('posted',$posted)->with('action',$action); 
	}else{
		return Redirect::back();
	}

}


	public function payment_old(){

		$save = Input::get('save');

		if($save){
			
			save_request();
		}

		return View::make('website.complete_payment'); 

	}

	public function success_payment()
	{
		
		$info = Input::get('productinfo');
		$get_id = explode('.',$info);
		$req_id = $get_id[1];
		

		$mihpayid = Input::get('mihpayid');
		$mode = Input::get('mode');
		$status = Input::get('status');
		$unmappedstatus = Input::get('unmappedstatus');
		$amount = Input::get('amount');
		$txnid = Input::get('txnid');
		$payment_msg = Input::get('field9');
		$pg_type = Input::get('PG_TYPE');
		$error_code = Input::get('error');
		$error_msg = Input::get('error_Message');
		$payuMoneyId = Input::get('payuMoneyId');
		$bank_ref_num = Input::get('bank_ref_num');
		$bankcode = Input::get('bankcode');

		$request = Requests::find($req_id);
		if($request->txnid === $txnid){
			$request->is_paid = 1;
			$request->save();
		}

		$payment = new Payment;
		if($mihpayid){
			$payment->mihpayid = $mihpayid;
		}
		if($mode){
			$payment->mode = $mode;
		}
		if($status){
			$payment->status = $status;
		}
		if($unmappedstatus){
			$payment->unmappedstatus = $unmappedstatus;
		}
		if($req_id){
			$payment->req_id = $req_id;
		}
		if($amount){
			$payment->amount = $amount;
		}
		if($txnid){
			$payment->txnid = $txnid;
		}
		if($payment_msg){
			$payment->payment_msg = $payment_msg;
		}
		if($pg_type){
			$payment->pg_type = $pg_type;
		}
		if($error_code){
			$payment->error_code = $error_code;
		}
		if($error_msg){
			$payment->error_msg = $error_msg;
		}
		if($bank_ref_num){
			$payment->bank_ref_num = $bank_ref_num;
		}
		if($bankcode){
			$payment->bankcode = $bankcode;
		}
		if($payuMoneyId){
			$payment->payuMoneyId = $payuMoneyId;
		}

		$payment->save();
		
		return View::make('website.success_payment');
	}

	public function failure_payment()
	{
		$info = Input::get('productinfo');
		$get_id = explode('.',$info);
		
		$req_id = $get_id[1];
		$mihpayid = Input::get('mihpayid');
		$mode = Input::get('mode');
		$status = Input::get('status');
		$unmappedstatus = Input::get('unmappedstatus');
		$amount = Input::get('amount');
		$txnid = Input::get('txnid');
		$payment_msg = Input::get('field9');
		$pg_type = Input::get('PG_TYPE');
		$error_code = Input::get('error');
		$error_msg = Input::get('error_Message');
		$payuMoneyId = Input::get('payuMoneyId');
		$bank_ref_num = Input::get('bank_ref_num');
		$bankcode = Input::get('bankcode');


		$payment = new Payment;
		if($mihpayid){
			$payment->mihpayid = $mihpayid;
		}
		if($mode){
			$payment->mode = $mode;
		}
		if($status){
			$payment->status = $status;
		}
		if($unmappedstatus){
			$payment->unmappedstatus = $unmappedstatus;
		}
		if($req_id){
			$payment->req_id = $req_id;
		}
		if($amount){
			$payment->amount = $amount;
		}
		if($txnid){
			$payment->txnid = $txnid;
		}
		if($payment_msg){
			$payment->payment_msg = $payment_msg;
		}
		if($pg_type){
			$payment->pg_type = $pg_type;
		}
		if($error_code){
			$payment->error_code = $error_code;
		}
		if($error_msg){
			$payment->error_msg = $error_msg;
		}
		if($bank_ref_num){
			$payment->bank_ref_num = $bank_ref_num;
		}
		if($bankcode){
			$payment->bankcode = $bankcode;
		}
		if($payuMoneyId){
			$payment->payuMoneyId = $payuMoneyId;
		}

		$payment->save();
		
		return View::make('website.failure_payment');
		
	}



}
