<?php

class AdminController extends Controller {

	public function __construct() {
        $this->beforeFilter(function () {
            if (!Auth::admin()->check()) {
                $url = URL::current();
                $routeName = Route::currentRouteName();
                Log::info('current route =' . print_r(Route::currentRouteName(), true));

                if ($routeName != "AdminLogin" && $routeName != 'admin') {
                   
                  
                }
                return Redirect::to('/admin/login');
            }
        }, array('except' => array('login', 'verify')));
    }


	public function login()
	{

		$admin=Admin::count();
		if($admin)
		{
		return View::make('admin.login')->with('button','Login');
			
		}else{
		return View::make('admin.login')->with('button','Create');
			
		}
	}

	public function verify()
	{
		$email = Input::get('email');
		$password = Input::get('password');
		$new = Input::get('new');

		$admin=Admin::count();
		if(!$admin && $new)
		{
			$admin = new Admin;
			$admin->email = $email;
			$admin->password = Hash::make($password);
			$admin->save();

			return Redirect::to('admin/login');

		}else{
			if (Auth::admin()->attempt(array('email' => $email, 'password' => $password))) {
				if (Session::has('pre_admin_login_url')) {
                    return Redirect::to('/admin/dashboard');
                } else {
                    return Redirect::to('/admin/dashboard');
                }
			}else{
				return Redirect::to('/admin/login?error=1');
			}
			
		}	
	}


	public function dashboard()
	{
		$request = Requests::orderBy('id','desc')->get();

		return View::make('admin.dashboard')->with('title','Dashboard')->with('request',$request);
	}

	public function logout()
	{
		Auth::admin()->logout();

		return Redirect::to('admin/login');
	}

	public function add_brewers()
	{
		return View::make('admin/add_brewers')->with('title','Add Brewers');
	}

	public function save_brewers()
	{
		$heading = Input::get('heading');
		$banner_img = Input::file('banner_img');

		$brewer = new Brewer;
		$brewer->heading = $heading;

		if (Input::hasFile('banner_img')) {
 		if ($brewer->banner_img != "") {
                    $path = $brewer->banner_img;
                    $filename = basename($path);
                    unlink(public_path() . "/uploads/" . $filename);
                }

				$file_name = time();
                $file_name .= rand();
                $ext = Input::file('banner_img')->getClientOriginalExtension();
                Input::file('banner_img')->move(public_path() . "/uploads", $file_name . "." . $ext);
                $local_url = $file_name . "." . $ext;

                    $s3_url = asset_url() . '/uploads/' . $local_url;
                
                $brewer->banner_img = $s3_url;
            }

        $brewer->save();

		return Redirect::to('/admin/list_brewers');
	}


	public function list_brewers()
	{
		$brewers = Brewer::all();
 		return View::make('admin.list_brewers')->with('brewers',$brewers)->with('title','List Brewers');
	}

	public function edit_brewer()
	{
		$id=Request::segment(3);
		$brewer = Brewer::find($id);
		return View::make('admin.edit_brewer')->with('brewer',$brewer)->with('title','Edit Brewers');
	}


	public function update_brewer()
	{
		$id = Input::get('id');
		$heading = Input::get('heading');
		$banner_img = Input::file('banner_img');

		$brewer =Brewer::find($id);
		$brewer->heading = $heading;

		if (Input::hasFile('banner_img')) {
 		if ($brewer->banner_img != "") {
                    $path = $brewer->banner_img;
                    $filename = basename($path);
                    unlink(public_path() . "/uploads/" . $filename);
                }

				$file_name = time();
                $file_name .= rand();
                $ext = Input::file('banner_img')->getClientOriginalExtension();
                Input::file('banner_img')->move(public_path() . "/uploads", $file_name . "." . $ext);
                $local_url = $file_name . "." . $ext;

                    $s3_url = asset_url() . '/uploads/' . $local_url;
                
                $brewer->banner_img = $s3_url;
            }

        $brewer->save();

		return Redirect::to('/admin/list_brewers');
	}

	public function delete_brewer()
	{
		$id = Request::segment(3);

		$count = Item::where('brewer_id',$id)->count();
		if($count){
				
		}else{

		Brewer::where('id',$id)->delete();
		}


		return Redirect::to('/admin/list_brewers');
	}

	public function add_item()
	{
		$brewers = Brewer::all();
		return View::make('admin/add_item')->with('brewers',$brewers)->with('title','Add Items');
	}

	public function list_items()
	{
		$items = Item::all();
 		return View::make('admin.list_items')->with('items',$items)->with('title','List Items');
	}

	public function save_item()
	{
		

		$brewer_id = Input::get('brewer_id');
		$name = Input::get('name');
		$price = Input::get('price');

		$item = new Item;
		$item->brewer_id = $brewer_id;
		$item->name = $name;
		$item->price = $price;


		if (Input::hasFile('image')) {
 		if ($item->image != "") {
                    $path = $item->image;
                    $filename = basename($path);
                    unlink(public_path() . "/uploads/" . $filename);
                }

				$file_name = time();
                $file_name .= rand();
                $ext = Input::file('image')->getClientOriginalExtension();
                Input::file('image')->move(public_path() . "/uploads", $file_name . "." . $ext);
                $local_url = $file_name . "." . $ext;

                    $s3_url = asset_url() . '/uploads/' . $local_url;
                
                $item->image = $s3_url;
            }

        $item->save();    

        return Redirect::to('/admin/list_items');

	}

	public function edit_item()
	{
		$id=Request::segment(3);
		$item = Item::find($id);
		return View::make('admin.edit_item')->with('item',$item)->with('title','Edit Items');
	}

	public function update_item()
	{
		$id = Input::get('id'); 
		$name = Input::get('name');
		$price = Input::get('price');

		$item =Item::find($id);
		$item->name = $name;
		$item->price = $price;


		if (Input::hasFile('image')) {
 		if ($item->image != "") {
                    $path = $item->image;
                    $filename = basename($path);
                    unlink(public_path() . "/uploads/" . $filename);
                }

				$file_name = time();
                $file_name .= rand();
                $ext = Input::file('image')->getClientOriginalExtension();
                Input::file('image')->move(public_path() . "/uploads", $file_name . "." . $ext);
                $local_url = $file_name . "." . $ext;

                    $s3_url = asset_url() . '/uploads/' . $local_url;
                
                $item->image = $s3_url;
            }

        $item->save();    

        return Redirect::to('/admin/list_items');

	}


	public function delete_item()
	{
		$id = Request::segment(3);

		Item::where('id',$id)->delete();

		return Redirect::to('/admin/list_items');
	}


	public function list_users()
	{
		$users = User::all();
 		return View::make('admin.list_users')->with('users',$users)->with('title','List Users');
	}


	public function user_list_items(){
		$id = Request::segment(3);
		$request = Requests::find($id);
		$ids = $request->items;

		$products = array();
		$exp = explode(',',$ids);
		foreach ($exp as $key ) {
				$item = array();
				$sep = explode('.', $key);
				$get_item = Item::where('id',$sep[0])->first();
				$item['id'] = $get_item->id;
				$item['name'] = $get_item->name;
				$item['image'] = $get_item->image;
				$item['price'] = $get_item->price;
				$item['quantity'] = $sep[1];
				array_push($products,$item);
			}

		return View::make('admin.user_list_item')->with('title','Users Order Items')->with('products',$products);
	}

}