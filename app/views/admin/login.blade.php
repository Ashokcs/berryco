<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="/website/fevicon/favicon.ico">

    <title>Login - Admin Panel</title>

    <!-- Bootstrap CSS -->    
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="/admin/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="/admin/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="/admin/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="/admin/css/style.css" rel="stylesheet">
    <link href="/admin/css/style-responsive.css" rel="stylesheet" />

</head>

  <body class="login-img3-body">

    <div class="container">

      <form class="login-form" method="get" action="{{URL::Route('AdminVerify')}}" >        
        <div class="login-wrap">
            <p class="login-img"><img style="width:50%;height:20%;" src="/website/images/demo/logo.png"></img></p>
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="text" name="email" class="form-control" placeholder="Username" required>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" name="password" class="form-control" placeholder="Password" required>
            </div>
             <?php if($button == 'Create'){  ?>
                    <input type="hidden" name="new" value="1">
            <?php  }else{ ?>
                    <input type="hidden" name="new" value="0">
           <?php }?>
            <button class="btn btn-primary btn-lg btn-block" type="submit">{{$button}}</button>
        </div>
      </form>

    </div>


  </body>
</html>
