@extends('admin.layout')

@section('content')
<a class="btn btn-primary" style="width: 10%; margin-left:88%; margin-top:-37px;" href="{{URL::Route('AdminAddItems')}}"><i class="icon_plus_alt2"></i>Add</a>
<div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              List Items
                          </header>
                          
                          <table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <!-- <th><i class="icon_profile"></i> Id</th> -->
                                 <th><i class=""></i> Brewer Heading</th>
                                 <th><i class=""></i> Item Name</th>
                                 <th><i class=""></i> Item Price</th>
                                 <th><i class=""></i> Image</th>
                                 <th><i class=""></i> Action</th>
                              </tr>
                              @foreach($items as $item)
                              <tr>
                                 <!-- <td>{{$item->id}}</td> -->
                                 <td>{{get_brewer_heading($item->brewer_id)}}</td>
                                 <td>{{$item->name}}</td>
                                 <td>Rs {{get_number_format($item->price,2)}}</td>
                                 <td><img src="<?= $item->image ?>" height="50" width="50"></td>
                                 <td>
                                  <div class="btn-group">
                                      <a class="btn btn-success" href="{{URL::Route('AdminEditItem',$item->id)}}"><i class="icon_check_alt2"></i></a>
                                      <a class="btn btn-danger" href="{{URL::Route('AdminDeleteItem',$item->id)}}" onclick="return confirm('Are you sure you want to delete this item?');"><i class="icon_close_alt2"></i></a>
                                  </div>
                                  </td>
                              </tr>
                              @endforeach                         
                           </tbody>
                        </table>
                      </section>
                  </div>
              </div>




@stop