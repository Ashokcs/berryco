@extends('admin.layout')

@section('content')

<div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              List Users
                          </header>
                          
                          <table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <!-- <th><i class="icon_profile"></i> Id</th> -->
                                 <th><i class="icon_calendar"></i> Name</th>
                                 <th><i class="icon_mail_alt"></i> Email</th>
                                 <th><i class="icon_mail_alt"></i> Phone</th>
                                 <th><i class="icon_mail_alt"></i> Address</th>
                                 <th><i class="icon_mail_alt"></i> Pincode</th>
                                 <th><i class="icon_key_alt"></i> Login_by</th>
                              </tr>
                              @foreach($users as $user)
                              <tr>
                                 <!-- <td>{{$user->id}}</td> -->
                                 <td>{{$user->name}}</td>
                                 <td>{{$user->email}}</td>
                                 <td>{{$user->phone}}</td>
                                 <td>{{$user->address}}</td>
                                 <td>{{$user->pincode}}</td>
                                 <td>{{$user->login_by}}</td>
                              </tr>
                              @endforeach                         
                           </tbody>
                        </table>
                      </section>
                  </div>
              </div>




@stop