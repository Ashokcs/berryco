@extends('admin.layout')

@section('content')
<a class="btn btn-primary" style="width: 10%; margin-left:88%; margin-top:-37px;" href="{{URL::Route('AdminAddBrewers')}}"><i class="icon_plus_alt2"></i>Add</a>
<div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              List Brewers
                          </header>
                          
                          <table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <!-- <th><i class="icon_profile"></i> Id</th> -->
                                 <th><i class="icon_calendar"></i> Heading</th>
                                 <th><i class="icon_mail_alt"></i> Banner Image</th>
                                 <th><i class="icon_cogs"></i> Action</th>
                              </tr>
                              @foreach($brewers as $brewer)
                              <tr>
                                 <!-- <td>{{$brewer->id}}</td> -->
                                 <td>{{$brewer->heading}}</td>
                                 <td><img src="<?= $brewer->banner_img ?>" height="50" width="50"></td>
                                 <td>
                                  <div class="btn-group">
                                      <a class="btn btn-success" href="{{URL::Route('AdminEditBrewer',$brewer->id)}}"><i class="icon_check_alt2"></i></a>
                                      <a class="btn btn-danger" href="{{URL::Route('AdminDeleteBrewer',$brewer->id)}}" onclick="return confirm('Are you sure you want to delete this item?');"><i class="icon_close_alt2"></i></a>
                                  </div>
                                  </td>
                              </tr>
                              @endforeach                         
                           </tbody>
                        </table>
                      </section>
                  </div>
              </div>




@stop