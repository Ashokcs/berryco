 @extends('admin.layout')

@section('content')


 <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                             Add Items
                          </header>
                          <div class="panel-body">
                              <div class="form">
                                  <form class="form-validate form-horizontal" method="post" action="{{URL::Route('AdminUpdateItem')}}" enctype="multipart/form-data" >
                                      <input type="hidden" name="id" value="<?= $item->id ?>" />
                                      
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Item Name <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" name="name" type="text" value="<?= $item->name ?>" required />
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Item Price <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" name="price" type="text" value="<?= $item->price ?>" required />
                                          </div>
                                      </div>                                      
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Item Image <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" name="image" type="file" />
                                          </div>
                                      </div>
                                     <img src="<?= $item->image ?>" height="50" width="50">                                     
                                     
                                      <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <button class="btn btn-primary" type="submit">Add</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>

                          </div>
                      </section>
                  </div>
              </div>


@stop
