 @extends('admin.layout')

@section('content')


 <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                             Add Items
                          </header>
                          <div class="panel-body">
                              <div class="form">
                                  <form class="form-validate form-horizontal" method="post" action="{{URL::Route('AdminSaveItem')}}" enctype="multipart/form-data" >
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Brewer <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                             <select name="brewer_id" class="form-control input-sm m-bot15">
                                          @foreach($brewers as $brewer)
                                              <option value="<?= $brewer->id; ?>" >{{$brewer->heading}}</option>
                                          @endforeach
                                          </select>
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Item Name <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" name="name" type="text" required />
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Item Price <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" name="price" type="text" required />
                                          </div>
                                      </div>                                      
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Item Image <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" name="image" type="file" required />
                                          </div>
                                      </div>
                                     
                                      <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <button class="btn btn-primary" type="submit">Add</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>

                          </div>
                      </section>
                  </div>
              </div>


@stop
