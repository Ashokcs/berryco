@extends('admin.layout')

@section('content')

<div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              List of Orders
                          </header>
                          
                          <table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <!-- <th><i class="icon_profile"></i> Id</th> -->
                                 <th><i class=""></i>Request ID</th>
                                 <th><i class=""></i> Name</th>
                                 <th><i class=""></i> Email</th>
                                 <th><i class=""></i> Phone</th>
                                 <th><i class=""></i> Address</th>
                                 <th><i class=""></i> City</th>
                                 <th><i class=""></i> State</th>
                                 <th><i class=""></i> Pincode</th>
                                 <th><i class=""></i> Amount</th>
                                 <th><i class=""></i> Is Paid</th>
                                 <th><i class=""></i> Items</th>
                              </tr>
                              
                              <tr>
                                @foreach($request as $req) 
                                 <td>{{$req->id}}</td>
                                 <td>{{$req->name}}</td>
                                 <td>{{$req->email}}</td>
                                 <td>{{$req->phone}}</td>
                                 <td>{{$req->address}}</td>
                                 <td>{{$req->city}}</td>
                                 <td>{{$req->state}}</td>
                                 <td>{{$req->pincode}}</td>
                                 <td>{{$req->total}}</td>
                                 <td>{{get_pay_status($req->is_paid)}}</td>
                                 <td><a href="{{URL::Route('AdminUserListItems',$req->id)}}"><span class ="btn-sm btn-info">view</span></a></td>
                              </tr>
                                @endforeach              
                           </tbody>
                        </table>
                      </section>
                  </div>
              </div>







@stop