@extends('admin.layout')

@section('content')

<a class="btn btn-primary" style="width: 10%; margin-left:88%; margin-top:-37px;" href="{{URL::Route('AdminDasboard')}}"><i class=""></i>Back</a>

<div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              List Brewers
                          </header>
                          
                          <table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <th><i class=""></i> Name</th>
                                 <th><i class=""></i> Image</th>
                                 <th><i class=""></i> Price</th>
                                 <th><i class=""></i> Quantity</th>
                              </tr>
                              @foreach($products as $prod)
                              <tr>
                                 <td>{{$prod['name']}}</td>
                                 <td><img src="<?= $prod['image'] ?>" height="50" width="50"></td>
                                 <td>{{$prod['price']}}</td>
                                 <td>{{$prod['quantity']}}</td>
                                 
                              </tr>
                              @endforeach                         
                           </tbody>
                        </table>
                      </section>
                  </div>
              </div>


@stop