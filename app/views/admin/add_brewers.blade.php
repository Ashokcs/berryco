 @extends('admin.layout')

@section('content')


 <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                             Add Brewers
                          </header>
                          <div class="panel-body">
                              <div class="form">
                                  <form class="form-validate form-horizontal" method="post" action="{{URL::Route('AdminSaveBrewers')}}" enctype="multipart/form-data" >
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Heading <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" name="heading" type="text" required />
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Banner Image <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" name="banner_img" type="file" required />
                                          </div>
                                      </div>                                      
                                     
                                      <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <button class="btn btn-primary" type="submit">Add</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>

                          </div>
                      </section>
                  </div>
              </div>


@stop
