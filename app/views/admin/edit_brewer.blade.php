 @extends('admin.layout')

@section('content')


 <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                            Edit Brewers
                          </header>
                          <div class="panel-body">
                              <div class="form">
                                  <form class="form-validate form-horizontal" method="post" action="{{URL::Route('AdminUpdateBrewer')}}" enctype="multipart/form-data" >
                                      <input type="hidden" name="id" value="{{$brewer->id}}"  />
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Heading <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" name="heading" type="text" value="{{$brewer->heading}}" required />
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="cname" class="control-label col-lg-2">Banner Image <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" name="banner_img" type="file" />
                                          </div>
                                      </div> 
                                      <img src="<?= $brewer->banner_img ?>" height="50" width="50">                                     
                                     
                                      <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-10">
                                              <button class="btn btn-primary" type="submit">Update</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>

                          </div>
                      </section>
                  </div>
              </div>


@stop
