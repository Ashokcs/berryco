<!DOCTYPE html>
<html>
<head>
<title>BerryCo-We Brew Solutions, Not Just Coffee.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="/website/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<link rel="apple-touch-icon" sizes="180x180" href="/website/fevicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/website/fevicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/website/fevicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/website/fevicon/manifest.json">
<link rel="mask-icon" href="/website/fevicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/website/fevicon/favicon.ico">
<meta name="msapplication-config" content="/website/fevicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
</head>
<body id="top">

<!-- Top Background Image Wrapper -->
<div class="row100 bgded" style="background-image:url('/website/images/demo/backgrounds/01.png');">
  <div class="overlay"> 
 
     <div class="wrapper">
      <header id="header" class="clear"> 
      
        <div id="logo" class="fl_left">
         <h1><a href="{{URL::Route('WebIndex')}}"><img src="/website/images/demo/logo1.png" alt="logo"</a></h1>
        </div>
    
     
        <div id="qct" class="fl_right">
         <?php if(!Auth::user()->check()) { ?>
          <ul class="nospace inline pushright">
            <li><a class="btn bold" href="{{URL::Route('WebLogin')}}" >Login</a></li> 
      
            <li><a class="btn bold" href="{{URL::Route('WebSignup')}}">Sign up</a></li>
          </ul>
          <?php }else{ ?>
          <ul class="nospace inline pushright">
            <li><i class=""></i><span> Welcome </span></li> &nbsp;
            <li><i class=""></i> <a href="">{{get_name(Auth::user()->id())}}</a></li>
            <li><a class="" href="{{URL::Route('WebLogout')}}">Logout</a></li>
          </ul>
          <?php } ?>
        </div>
      
      </header>
    </div>
  
    <div class="wrapper padbot">
      <nav id="mainav">
        <ul class="clear">
        <li><a href="{{URL::Route('WebIndex')}}">&nbsp;&nbsp;Home</a></li>
          <li><a href="{{URL::Route('WebAbout')}}">&nbsp;&nbsp;About Us</a></li>
     <li><a  href="{{URL::Route('WebEqu')}}">Coffee Brewers<sup style="color:red;font-size:7pt;">New</sup>  </a>
           
          </li>
          <li><a class="drop" href="{{URL::Route('WebService')}}">&nbsp;&nbsp;Services</a>
            <ul>
              <li><a href="{{URL::Route('WebService')}}#at_estate">At Estate</a></li>
              
              <li><a href="{{URL::Route('WebService')}}#qulality">Quality Assurance</a></li>
              <li><a href="{{URL::Route('WebService')}}#roaster">For Roasters</a></li>
        <li><a href="{{URL::Route('WebService')}}#training">Barista and Training</a></li>
        <li><a href="{{URL::Route('WebService')}}#Business">Business Development and entrepreneurship</a></li>
            </ul>
          </li>
          <li class="active"><a href="{{URL::Route('WebBlog')}}">&nbsp;&nbsp;Blog</a></li>
          <li><a href="{{URL::Route('WebFaq')}}">&nbsp;&nbsp;FAQ</a></li>
              
              <li><a href="{{URL::Route('WebWeare')}}">&nbsp;&nbsp;Who we are?</a></li>
              
      
      <li><a href="{{URL::Route('WebQur')}}">&nbsp;&nbsp;Queries?</a></li> 
        </ul>
      </nav>
    </div>
 
  </div>
</div>
<!-- End Top Background Image Wrapper --> 

<div class="wrapper row2">
  <div id="breadcrumb" class="clear"> 
  
    <ul>
      <li><a href="{{URL::Route('WebIndex')}}">Home</a></li>
      <li><a href="#">Blog</a></li>
    </ul>
  
  </div>
</div>

<div class="wrapper row3">
  <main class="container clear"> 
    <!-- main body --> 
   
    <div class="content "> 

      
      <div id="comments">
        <h2><b>Blog</b></h2>
    
        <ul>
          <li>
            <article>
              <header>
        <h2>CUP OF JOY</h2>
                <figure class="avatar"><img src="/website/images/demo/avatar.png" alt=""></figure>
                <address>
                By <a href="#">Ayesha C
        </a>
                </address>
                <time datetime="2050-04-06T08:15+00:00">Monday, 23<sup>rd</sup> November 2015 @09:15:00</time>
              </header>
              <div class="comcont">
        <p><img class="imgl borderedbox inspace-5" src="/website/images/demo/blog1.jpg">
                Someone has very rightly said…Coffee is nothing but a hug in a mug. A cup of coffee shared with a friend or a loved one is always happiness shared and time very well spent. Who doesn’t love to wrap their hands around a cup of coffee and swallow it... whether it’s a cold Monday morning or a silent yawn, to pull up your socks and start working or to just relax? Who doesn’t reach for one..
        </p>        Years ago…treated as an appetite suppressant; it was discovered by Kaldi an Ethiopian goatherd when he noticed how excited his goats became after eating the beans from a coffee plant.
        <br/>
        From Middle East, coffee travelled its way to different countries discovering new identities. A German physician Leonhard Rauwolf gave this prescription of coffee after returning from ten year trip to east.<br/>
        <br/>
        A beverage as black as ink, useful against numerous illnesses, particularly those of the stomach. Its consumers take it in the morning, quite frankly, in a porcelain cup that is passed around and from which each one drinks a cupful. It is composed of water and the fruit from a bush called bunnu.<br/>
        <br/>
        <i>— Léonard Rauwolf, Reise in die Morgenländer (in German)</i><br/>
<br/>
 <p><img class="imgl borderedbox inspace-5" src="/website/images/demo/blog2.jpg">
How cool is that now!!!<br/>
There have been lots of discussions as to how much of it is too good to swallow. But, we all know coffee now is another family member in our household. Which is, behind every woman who has to struggle through her day to complete her chores? To help you fight that sleep, so that you can burn the midnight oil. To get you some time to bond with that special person. Or just an excuse to meet that old friend. Any excuse, anytime, a cup of joy is well deserved by all!<br/>
</p>
              </div>
            </article>
          </li>
          
        </ul>
        
      </div>

    </div>
  
    <!-- / main body -->
    <div class="clear"></div>
  </main>
  <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>
</div>


<div class="wrapper row4">
  <footer id="footer" class="clear"> 

    <div class="one_quarter first">
      <h6 class="title">Company Details</h6>
      <address class="btmspace-15">
      #1, Bhadrappa Layout,1st Main,1st cross, <br>
   Kodigehalli Main Road,<br>
   Near Big Bazar<br>
     Bangalore-560094<br/>
   India
      </address>
      <ul class="nospace">
        <li class="btmspace-10"><span class="fa fa-phone"></span>+91 (886) 122 1284</li>
    <li class="btmspace-10"><span class="fa fa-phone"></span>+91 (966) 373 0974</li>
        <li><span class="fa fa-envelope-o"></span> mail@berryco.info</li>
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="title">From The Blog</h6>
      <article>
        <h2 class="nospace font-x1"><a href="#">CUP OF JOY</a></h2>
        <time class="smallfont" datetime="2045-04-06">Monday, 23<sup>rd</sup> November 2015</time>
        <p>Someone has very rightly said…Coffee is nothing but a hug in a mug. A cup of coffee shared with a friend or a loved one is always happiness shared and time very well spent... </p>
      </article>
    </div>
    
  <div class="one_quarter">
      <h6 class="title">Our Belief</h6>
      <p>We believe that qualitity is the only undying method of building trust and retaining customer. </p>
      <p>When you change the was look at things, the things you look at will change-Wayne Dyer </p>
    </div>
  
  
    <div class="one_quarter">
      <h6 class="title">Our Service</h6>
      <p>We at BerryCo believe coffee is more then just a drink and would rediscover the value around your coffee. </p>
      <p>Drown your troubles in us and we develop strategic solutions and guidance on the path to success.</p>
    </div>

  </footer>
</div>

<div class="wrapper">
  <div id="socialblock" class="clear"> 
   
    <ul class="faico clear">
     <li><a class="faicon-facebook" href="https://www.facebook.com/BerryCo-506568626176433/"><i class="fa fa-facebook"></i></a></li>
      <li><a class="faicon-twitter" href="https://twitter.com/BerryCocoffee"><i class="fa fa-twitter"></i></a></li>
      <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
      <li><a class="faicon-google-plus" href="https://plus.google.com/u/0/104255358066214176181/about"><i class="fa fa-google-plus"></i></a></li>
    <li><a class="faicon-rss" href="https://www.addtoany.com/share?linkurl=www.berryco.info&amp;linkname="><i class="fa fa-share-alt"></i></a></li>
<!-- AddToAny BEGIN -->
<script type="text/javascript">
var a2a_config = a2a_config || {};
a2a_config.linkurl = "www.berryco.info";
a2a_config.onclick = 1;
</script>
<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->
    </ul>
  
  </div>
</div>

<div class="wrapper row5">
  <div id="copyright" class="clear"> 

    <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="berryco.info">BerryCo.info</a></p>

  </div>
</div>
 
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a> 
<!-- JAVASCRIPTS --> 
<script src="/website/layout/scripts/jquery.min.js"></script> 
<script src="/website/layout/scripts/jquery.backtotop.js"></script> 
<script src="/website/layout/scripts/jquery.mobilemenu.js"></script> 
<!-- IE9 Placeholder Support --> 
<script src="/website/layout/scripts/jquery.placeholder.min.js"></script> 
<!-- / IE9 Placeholder Support -->
</body>
</html>