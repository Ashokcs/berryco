
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BerryCo-We Brew Solutions, Not Just Coffee.</title>
    
    <!-- Google Fonts -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    
    <!-- Bootstrap -->
    <!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"> -->
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    
    <!-- Custom CSS -->
	<link href="/website/css/layout.css" rel="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" href="/website/css/owl.carousel.css">
    <link rel="stylesheet" href="/website/css/style-checkout.css">
    <link rel="stylesheet" href="/website/css/responsive.css">

   
  </head>
  <body style="background: transparent;">
   
   <!-- Top Background Image Wrapper -->
	<div class="row100 bgded" style="background-image:url('/website/img/01.png');">
		<div class="overlay">
			<div class="wrapper">
				<!-- Header -->
				<header id="header" class="clear"> 
					<div id="logo" class="fl_left">
						<h1><a href="{{URL::Route('WebIndex')}}"><img src="/website/img/logo1.png" alt="logo"></a></h1>
					</div>
					<div id="qct" class="fl_right">
                     <?php if(!Auth::user()->check()) { ?>
                      <ul class="nospace inline pushright">
                        <li><a class="btn bold" href="{{URL::Route('WebLogin')}}" >Login</a></li> 
                        
                        <li><a class="btn bold" href="{{URL::Route('WebSignup')}}">Sign up</a></li>
                      </ul>
                      <?php }else{ ?>
                      <ul class="nospace inline pushright">
                        <li><i class=""></i><span> Welcome </span></li> &nbsp;
                        <li><i class=""></i> <a href="">{{get_name(Auth::user()->id())}}</a></li>
                        <li><a class="" href="{{URL::Route('WebLogout')}}">Logout</a></li>
                      </ul>
                      <?php } ?>
                    </div>
				</header>
			</div>
			
			<!-- Wrapper -->
			<div class="wrapper padbot">
				<nav id="mainav">
					<ul class="clear">
						<li><a href="{{URL::Route('WebIndex')}}">Home</a></li>
						<li><a href="{{URL::Route('WebAbout')}}">About Us</a></li>
						<li class="active">
							<a  href="{{URL::Route('WebEqu')}}">Coffee Brewers<sup style="color:red;font-size:8pt;">New</sup></a>
						</li>
						<li>
							<a class="drop" href="{{URL::Route('WebService')}}">&nbsp;&nbsp;Services</a>
							<ul>
								<li><a href="{{URL::Route('WebService')}}">At Estate</a></li>
								<li><a href="{{URL::Route('WebService')}}#qulality">Quality Assurance</a></li>
								<li><a href="{{URL::Route('WebService')}}#roaster">For Roasters</a></li>
								<li><a href="{{URL::Route('WebService')}}#training">Barista and Training</a></li>
								<li><a href="{{URL::Route('WebService')}}#Business">Business Development and entrepreneurship</a></li>
							</ul>
						</li>
						<li><a href="{{URL::Route('WebBlog')}}">&nbsp;&nbsp;Blog</a></li>
              <li><a href="{{URL::Route('WebFaq')}}">&nbsp;&nbsp;FAQ</a></li>
              <li><a href="{{URL::Route('WebWeare')}}">&nbsp;&nbsp;Who we are?</a></li>
          <li><a href="{{URL::Route('WebQur')}}">&nbsp;&nbsp;Queries?</a></li> 
					</ul>
				</nav>
			</div>
		</div>
	</div>
    
    
    <div class="single-product-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Your Products</h2>
                    <hr>
                    @foreach($items as $item)
                        <div class="thubmnail-recent">
                            <img src="{{$item['image']}}" style="width:85px;height:85px" class="recent-thumb" alt="">
                            <h2 style="color:#222" >{{$item['name']}}</h2>
                            <div class="product-sidebar-price">
                            <ins>{{$item['quantity']}}</ins>
                            </div>
                            <div class="product-sidebar-price">
                                <ins>Rs {{get_number_format($item['price'],2)}}</ins><a type="button" href="{{URL::Route('WebRemoveItem',$item['id'])}}" style="text-decoration:none;color:red">x Remove</a>
                            </div>                             
                        </div>
                    <hr>
                   @endforeach
                   <div class="thubmnail-recent">
                      <p><label style="color:#BE9145"> Subtotal : Rs {{$total}}</label></p>
                   </div>
                    </div>
                </div>
                
                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="woocommerce">
                            
                         <!--    <div class="woocommerce-info">Have a coupon? <a class="showcoupon" data-toggle="collapse" href="#coupon-collapse-wrap" aria-expanded="false" aria-controls="coupon-collapse-wrap">Click here to enter your code</a>
                            </div>

                            <div id="coupon-collapse-wrap" method="post" class="checkout_coupon collapse">

                                <p class="form-row form-row-first">
                                    <label >Address
                                    </label>
                                    <textarea  name="address" class="input-text"></textarea>
                                </p>
                                <p class="form-row form-row-first">
                                    <label for="username">Pincode 
                                    </label>
                                    <br>
                                    <input type="text" id="username" name="username" class="input-text">
                                </p>

                                <div class="clear"></div>
                            </div> -->

                            <form enctype="multipart/form-data" action="{{URL::Route('WebFinalCheck')}}" class="checkout" method="post">

                                <div id="customer_details" class="col2-set">
                                    <div class="col-1">
                                        <div class="woocommerce-billing-fields">
                                            <h3>Billing Details</h3>
                
                                            <div class="clear"></div>

                                            
                                            <p id="billing_address_1_field" class="form-row form-row-wide address-field validate-required">
                                                <label class="" for="billing_address_1">Your Name<abbr title="required" class="required">*</abbr>
                                                </label>
                                                <input type="text" value="<?= $user->name ?>" placeholder="Enter Your Name" name="name" class="input-text ">
                                            </p>
 
                                            <p id="billing_email_field" class="form-row form-row-first validate-required validate-email">
                                                <label class="" for="billing_email">Phone <abbr title="required" class="required">*</abbr>
                                                </label>
                                                <input type="text" value="<?= $user->phone ?>" placeholder="Enter Your Phone" name="phone" class="input-text ">
                                            </p>

                                            <p id="billing_phone_field" class="form-row form-row-last validate-required validate-phone">
                                                <label class="" for="billing_phone">Address <abbr title="required" class="required">*</abbr>
                                                </label>
                                                <textarea name="address" ><?= $user->address ?></textarea>
                                                <!-- <input type="text" value="<?= $user->address ?>" placeholder="Enter Your Address" name="address" class="input-text "> -->
                                            </p> 
                                            <p id="billing_phone_field" class="form-row form-row-last validate-required validate-phone">
                                                <label class="" for="billing_phone">Pincode <abbr title="required" class="required">*</abbr>
                                                </label>
                                                <input type="text" value="<?= $user->pincode ?>" placeholder="Enter Your Pincode" name="pincode" class="input-text ">
                                            </p>
                                            <div class="clear"></div>
                                        <input type="submit" data-value="Place order" value="Complete payment"  class="button alt">

                                        </div>
                                    </div>

                                </div>
                               
                                      
                            </form>

                        </div>                       
                    </div>                    
                </div>
            </div>
        </div>
    </div>

	
		<!-- Wrapper -->
	<div class="wrapper row4">
		<!-- Footer -->
		<footer id="footer" class="clear"> 
			<div class="one_quarter first">
				<h6 class="title">Company Details</h6>
				<address class="btmspace-15">
					#1, Bhadrappa Layout,1st Main,1st cross, <br>
					Kodigehalli Main Road,<br>
					Near Big Bazar<br>
					Bangalore-560094<br/>
					India
				</address>
				<ul class="nospace">
					<li class="btmspace-10"><span class="fa fa-phone"></span>+91 (886) 122 1284</li>
					<li class="btmspace-10"><span class="fa fa-phone"></span>+91 (966) 373 0974</li>
					<li><span class="fa fa-envelope-o"></span> mail@berryco.info</li>
				</ul>
			</div>
			
			<div class="one_quarter">
				<h6 class="title">From The Blog</h6>
				<article>
					<h2 class="nospace font-x1"><a href="#">CUP OF JOY</a></h2>
					<time class="smallfont" datetime="2045-04-06">Monday, 23<sup>rd</sup> November 2015</time>
					<p>Someone has very rightly said…Coffee is nothing but a hug in a mug. A cup of coffee shared with a friend or a loved one is always happiness shared and time very well spent... </p>
				</article>
			</div>
    
			<div class="one_quarter">
				<h6 class="title">Our Believe</h6>
				<p>We believe that qualitity is the only undying method of building trust and retaining customer. </p>
				<p>When you change the was look at things, the things you look at will change-Wayne Dyer </p>
			</div>
	
			<div class="one_quarter">
				<h6 class="title">Our Service</h6>
				<p>We at BerryCo believe coffee is more then just a drink and would rediscover the value around your coffee. </p>
				<p>Drown your troubles in us and we develop strategic solutions and guidance on the path to success.</p>
			</div>
		</footer>
	</div>

	<!-- Wrapper -->
	<div class="wrapper">
		<div id="socialblock" class="clear"> 
			<ul class="faico clear">
				<li><a class="faicon-facebook" href="https://www.facebook.com/BerryCo-506568626176433/"><i class="fa fa-facebook"></i></a></li>
				<li><a class="faicon-twitter" href="https://twitter.com/BerryCocoffee"><i class="fa fa-twitter"></i></a></li>
				<li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
				<li><a class="faicon-google-plus" href=""><i class="fa fa-google-plus"></i></a></li>
				<li><a class="faicon-rss" href=""><i class="fa fa-rss"></i></a></li>
			</ul>
		</div>
	</div>
	<div class="wrapper row5">
		<div id="copyright" class="clear"> 
			<p class="fl_left">Copyright &copy; 2015 - All Rights Reserved - <a href="#">Domain Name</a></p>
			<p class="fl_right">Template by <a target="_blank" href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
		</div>
	</div>
	
	<!-- Back to Top -->
	<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>

   
    <!-- Latest jQuery form server -->
    <script src="https://code.jquery.com/jquery.min.js"></script>
    
    <!-- Bootstrap JS form CDN -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    
    <!-- jQuery sticky menu -->
    <script src="/website/js/owl.carousel.min.js"></script>
    <script src="/website/js/jquery.sticky.js"></script>
    
    <!-- jQuery easing -->
    <script src="/website/js/jquery.easing.1.3.min.js"></script>
    
    <!-- Main Script -->
    <script src="/website/js/main.js"></script>
  </body>
</html>