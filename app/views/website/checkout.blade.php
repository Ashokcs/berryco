<!DOCTYPE html>
<html lang="en">
	<head>
		<!--meta-->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Checkout</title>
		
		<!-- css -->
		<link href="/website/checkout/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="/website/checkout/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!--<link href="css/default.css" rel="stylesheet" type="text/css">-->
		
		<!--Google font-->
		<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
		<link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
		
		<!--internal css-->
	    <style>
		
		.checkout-outer
		{
			font-family: 'Muli', sans-serif;
		}
		.user-details input[type="text"], input[type="passowrd"], input[type="email"]
		{
			background:#f1f1f1;
		}
		.user-details input[type="text"]:focus,
		 input[type="passowrd"]:focus, input[type="email"]:focus
		 {
			background:#fff;
		 }
		.checkout-outer a
		{
			text-decoration:none;
			color:#000;
		}
		.checkout-outer a:hover
		{
			text-decoration:none;
			color:#000;
		}
		.checkout-outer  .navbar
		{
			margin-top: 30px;
			border-radius: 0px;
			padding: 6px 0px;
		}
		.navbar-default .navbar-brand
		{
			font-size:19px;
			letter-spacing:0.3px;
			color:#000;
			font-weight:900;
			
		}
		.checkout-outer .checkout-inner p
		{
			text-transform:uppercase;
			margin-bottom:0px;
			font-family: 'Nunito', sans-serif ;
		}	
		.checkout-outer .checkout-inner label
		{
			color:#999;
			font-size:13px;
		}
		.checkout-outer .right_section
		{
			margin-bottom:30px;
		}
		.checkout-outer .right_section .panel		
		{
				margin-bottom: 6px;
		}
		.checkout-outer .right_section .list-group
		{
			margin-bottom:0px;
		}
		.checkout-outer .payment-type label
		{
			color:#000;
		}
		.checkout-outer .payment-type .green.box, .checkout-outer .payment-type .red.box, .checkout-outer .payment-type .blue.box
		{
			margin-top:20px;
		}
		.checkout-outer .payment-type .red.box p
		{
			margin-bottom:15px;
		}
		.checkout-outer .payment-type .red.box .btn
		{
			border-radius:0px;
			padding:5px 22px;
		}
		.checkout-outer .product_description
		{
			margin:10px 0px;
		}
		.checkout-outer  .review_img
		{
			text-align:center;
		}
		@media(max-width:330px)
		{
			.checkout-outer .checkout_radio
			{
				margin-left:2px;
			}
		}
		</style>
		<body>
			<!--outer-starts-->
			<div class="checkout-outer">
				<div class="container">
					<nav class="navbar navbar-default">
						<div class="navbar-header">
							<div class="navbar-brand">
							BERRYCO
							</div>
						</div>
					</nav>
					<form enctype="multipart/form-data" action="{{URL::Route('WebPayment')}}" method="post" >
					<div class="checkout-inner">
						<div class="row">

						<div class="panel panel-default">
										<div class="panel-heading">
										<a href="{{URL::Route('WebEqu')}}" class="btn-xs btn-info" style="float:right;" >+ add more</a>
											<p>Review Product</p>
										</div>
											 @foreach($items as $item)
										<div class="panel-body">
											<div class="row">
												<div class="col-lg-2 col-sm-2 col-md-3 col-xs-12">
													<div class="review_img">
														<img src="{{$item['image']}}" style="width:85px;height:85px" class="img-responsive"/>
													</div>
												</div>
												<div class="col-lg-6 col-sm-6 col-md-4 col-xs-12">
												<div class="product_description">
													<span>
														{{$item['name']}}<br/>
														Price: Rs {{get_number_format($item['price'],2)}}<br/>
														
													</span>
												</div>
												</div>
												<div class="col-lg-3 col-sm-3 col-md-4 col-xs-12">
												<div class="shipping_details">
													<span>
													<br/>
														<a href="{{URL::Route('WebRemoveItem',$item['id'])}}" class="btn btn-default">-</a>&nbsp;&nbsp;{{$item['quantity']}}&nbsp;&nbsp;<a href="{{URL::Route('WebBuyAddCart',$item['id'])}}"  class="btn btn-default">+</a> &nbsp;&nbsp;&nbsp;&nbsp;
														<a class="btn btn-danger" href="{{URL::Route('WebFullRemoveItem',$item['id'])}}">remove </a>
														
													</span>
												</div>
												</div>
											</div>
										</div>
												@endforeach
									</div>
							<div class="col-lg-8 col-sm-8">
								<div class="center_section">
									<!--panel-one-->
									<div class="panel panel-default">
										<div class="panel-heading">
											<p>Choose a shipping address</p>
										</div>
										<div class="panel-body">
											<div class="user-details">
												<div class="form-group">
												<input type="hidden" name="email" value="<?= get_email(); ?>" />
												<input type="hidden" name="productinfo" value="BerryCo product." />
												<input type="hidden" name="surl" value="{{URL::Route('WebPaySuccess')}}" />
												<input type="hidden" name="furl" value="{{URL::Route('WebPayFailure')}}" />
												<input type="hidden" name="save" value="1" />
													<label>Full Name </label>
													<input type="text" name="firstname" class="form-control" required />
												</div>
											</div>
											<div class="user-details">
												<div class="row">
													<div class="col-lg-6 col-sm-6">
														<div class="form-group">
															<label>Address Line 1 </label>
															<input type="text" name="address1" class="form-control" required>
														</div>
													</div>
													<div class="col-lg-6 col-sm-6">
														<div class="form-group">
															<label>Address Line 2</label>
															<input type="text" name="address2" class="form-control" required>
														</div>
													
													</div>
												</div>
											</div>
											<div class="user-details">
												<div class="row">
													<div class="col-lg-4 col-sm-4">
														<div class="form-group">
															<label>State</label>
															<input type="text" name="state" class="form-control" required />
														</div>
													</div>
													<div class="col-lg-3 col-sm-3">
														<div class="form-group">
															<label>City</label>
															<input type="text" name="city" class="form-control" required />
														</div>
													
													</div>
													<div class="col-lg-5 col-sm-5">
														<div class="form-group">
															<label>Pincode</label>
															<input type="text" name="zipcode" class="form-control" required />
														</div>
													</div>
												</div>
											</div>
											<div class="user-details">
												<div class="row">
													<div class="form-group col-lg-6 col-sm-6">
														<label>Phone</label>
														<input type="text" name="phone" class="form-control" required />
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--panel-two-->
									
									<!--panel-three-->
									<div class="panel panel-default">
										<div class="panel-heading">
											<p>Payment Method</p>
										</div>
										<div class="panel-body">
											<div class="payment-type">
												  <div class="red box">
												  	While clicking Proceed to Pay it will redirect to PayUmoney
													<!-- Click the button to proceed payment via PayUmoney -->
													<img src="/website/payumoneyLogo.png">
												  </div>
											</div>
										</div>
									</div>
									<!--panel four-->
									
								</div>
							</div>
							<div class="col-lg-4 col-sm-4">
								<div class="right_section">
									<div class="panel panel-default">
										<div class="panel-heading">
											<b>Order Summary</b>
										</div>
										<div class="panel-body">
											<div class="order-summary-inner">
												<ul class="list-group">
												  <li class="list-group-item">
													<span class=" pull-right">Rs {{get_number_format(get_subtotal(),2)}}</span>
													Subtotal
												  </li>
												  <li class="list-group-item">
													<span class="pull-right">- Rs 0</span>
													Discount
												  </li>
												  <li class="list-group-item">
													<span class=" pull-right">Rs 0</span>
													Shipping
												  </li>
												  <li class="list-group-item">
													<span class=" pull-right">Rs {{get_number_format(get_subtotal(),2)}}</span>
													<input type="hidden" name="amount" value="<?= get_subtotal(); ?>" />
													<b>Total</b>
												  </li>
												  <li class="list-group-item">
													<button type="submit" class="btn btn-info btn-block"><b>Proceed To Pay</b></button>
												  </li>
												</ul>
											</div>
										</div>
									</div>
									<span>By placing your order,you agree to Berryco Privarcy policy and terms and conditions</span>
								</div>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>

			<script src="/website/checkout/js/jquery.min.js"></script>
			<script src="/website/checkout/js/bootstrap.min.js"></script>
			<script>
				$(document).ready(function(){
					$(".green").hide();
					$(".red").hide();
					$(".blue").hide();
				});
			</script>
			<script type="text/javascript">
					$(document).ready(function(){
						$(".red").show();
					$('input[type="radio"]').click(function(){
						// if($(this).attr("value")=="red"){
							// $(".box").not(".red").hide();
							$(".red").show();
						// }
					});
				});
			</script>
	
	
	</body>
</html>
		