<!DOCTYPE html>

<html>
<head>
<title>BerryCo-We Brew Solutions, Not Just Coffee.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="/website/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
 <script type="text/javascript" src="/website/layout/scripts/smoothscroll.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="apple-touch-icon" sizes="180x180" href="/website/fevicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/website/fevicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/website/fevicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/website/fevicon/manifest.json">
<link rel="mask-icon" href="/website/fevicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/website/fevicon/favicon.ico">
<meta name="msapplication-config" content="/website/fevicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
</head>
<body id="top">

<!-- Top Background Image Wrapper -->
<div class="row100 bgded" style="background-image:url('/website/images/demo/backgrounds/01.png');">
  <div class="overlay"> 
   
     <div class="wrapper">
      <header id="header" class="clear"> 
     
        <div id="logo" class="fl_left">
         <h1><a href="{{URL::Route('WebIndex')}}"><img src="/website/images/demo/logo1.png" alt="logo"</a></h1>
        </div>
		
		 
       <div id="qct" class="fl_right">
         <?php if(!Auth::user()->check()) { ?>
          <ul class="nospace inline pushright">
            <li><a class="btn bold" href="{{URL::Route('WebLogin')}}" >Login</a></li> 
			
            <li><a class="btn bold" href="{{URL::Route('WebSignup')}}">Sign up</a></li>
          </ul>
          <?php }else{ ?>
          <ul class="nospace inline pushright">
            <li><i class=""></i><span> Welcome </span></li> &nbsp;
            <li><i class=""></i> <a href="">{{get_name(Auth::user()->id())}}</a></li>
          	<li><a class="" href="{{URL::Route('WebLogout')}}">Logout</a></li>
          </ul>
          <?php } ?>
        </div>
       
      </header>
    </div>
  
  
   
   <div class="wrapper padbot">
      <nav id="mainav">
        <ul class="clear">
          <li><a href="{{URL::Route('WebIndex')}}">&nbsp;&nbsp;Home</a></li>
          <li><a href="{{URL::Route('WebAbout')}}">&nbsp;&nbsp;About Us</a></li>
		 <li><a  href="{{URL::Route('WebEqu')}}">Coffee Brewers<sup style="color:red;font-size:7pt;">New</sup>	</a>
           
          </li>
          <li class="active"><a class="drop" href="{{URL::Route('WebService')}}">&nbsp;&nbsp;Services</a>
            <ul>
              <li><a href="{{URL::Route('WebService')}}#at_estate">At Estate</a></li>
              
              <li><a href="{{URL::Route('WebService')}}#quality">Quality Assurance</a></li>
              <li><a href="{{URL::Route('WebService')}}#roaster">For Roasters</a></li>
			  <li><a href="{{URL::Route('WebService')}}#training">Barista and Training</a></li>
			  <li><a href="{{URL::Route('WebService')}}#Business">Business Development and entrepreneurship</a></li>
            </ul>
          </li>
          <li><a href="{{URL::Route('WebBlog')}}">&nbsp;&nbsp;Blog</a></li>
		      <li><a href="{{URL::Route('WebFaq')}}">&nbsp;&nbsp;FAQ</a></li>
              
              <li><a href="{{URL::Route('WebWeare')}}">&nbsp;&nbsp;Who we are?</a></li>
              
			
		  <li><a href="{{URL::Route('WebQur')}}">&nbsp;&nbsp;Queries?</a></li>
		  
        </ul>
      </nav>
    </div>
   
  </div>
</div>
<!-- End Top Background Image Wrapper --> 

<div class="wrapper row2">
  <div id="breadcrumb" class="clear"> 
   
    <ul>
      <li><a href="/website/index.html">Home</a></li>
      
      <li><a href="{{URL::Route('WebService')}}">Services</a></li>
    </ul>
   
  </div>
</div>

<script language="javascript"> 
function toggle() {
	var ele = document.getElementById("toggleText");
	var text = document.getElementById("displayText");

	
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "know more..";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide";
		
	}

} 
function toggle1() {
	var ele = document.getElementById("toggleText1");
	var text = document.getElementById("displayText1");

	
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "know more..";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide";
	}

} 
function toggle2() {
	var ele = document.getElementById("toggleText2");
	var text = document.getElementById("displayText2");

	
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "know more..";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide";
	}

} 
function toggle3() {
	var ele = document.getElementById("toggleText3");
	var text = document.getElementById("displayText3");

	
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "know more..";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide";
	}

} 
function toggle4() {
	var ele = document.getElementById("toggleText4");
	var text = document.getElementById("displayText4");

	
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "know more..";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide";
	}

} 
</script>


<div class="wrapper row3">


  <main class="container clear"> 
    <!-- main body --> 
    
    <div class="content"> 
    
     <h1><b><font size="25"> Services</font></b></h1>
      
	  <div id = "at_estate">
	  <br/>
	  <br/>
<div class="wrapper row100 bgded" style="background-image:url('/website/images/demo/coffee1.jpg');">
  <div class="overlay">
    <article id="shout" class="capitalise clear"> 
    
      <center><h2 class="heading"><font size="20" >At Estate</font> </h2></center>
      <p><strong><a href="#"></a></strong></p>
     
    </article>
  </div>
</div>	  
	 <!-- SIZE CHANGE -->
 <font size ="3.5">

      <p>Managing a coffee estate can be an arduous task for coffee planters. Many planters face several problems such as pests, yield performance, farm operations including harvesting, picking and processing, curing to name a few. BerryCo seeks to help you manage these issues effectively and provide you the solutions that best suit your needs. Our work methodology involves active on-site visits to the estate in order to effectively understand the issues faced by the estate holders, farmers and other stakeholders. Based on a preliminary analysis of the estate, we will provide the client with a preliminary report.

	  <a id="displayText" href="javascript:toggle();">Know more..</a>
      <div id="toggleText" style="display: none">


	  <p>BerryCo will then identify the issues and provide a detailed analysis of the same. The next step to this would include providing the client the best approach to be adopted in countering the issues at hand and arrive at the best achievable results. We will also seek out the best ways to increase your coffee’s quality and yields by working closely with experts in the field who know their coffee and yours!! From picking coffee at the right time to milling and drying and every other process in line, we will provide you with solutions that will help you create the best product around. We would also love to help you design, build and staff that utopian facility, you have always desired for your coffee processing and distribution.</p>
      
      <p>BerryCo looks into all the aspects required for the successful maintenance of estates, including the economic, social and scientific aspects. We find the best solutions to increase your yield and ensure to work on judicious productive ways. Some of our services at the Estate level include but are not limited to:</p>
		 
	 <p>•	Advisory to the cultivation </p>
      <p>•	Analysis of farm operations </p>
	  <p>•	Plant health diagnosis</p>
	  <p>•	Coffee processing analysis</p>
	  <p>•	Quality assurance at estate level</p>
	  <p>•	Moisture analysis</p>
	  <p>•	Storage of green coffee bean</p>
	  <p>•	Storage of green coffee bean</p> </p>
	
</div>
	</div>

	 <br/>
	 <br/>
	 <br/>
	 
	  <div id="quality">
	 <br/>
<br/>	 
	  <div class="wrapper row100 bgded" style="background-image:url('/website/images/demo/coffee2.jpg');">
  <div class="overlay">
    <article id="shout" class="capitalise clear"> 
     
      <center><h2 class="heading"><font size="20" >Quality assurance</font> </h2></center>
      <p><strong><a href="#"></a></strong></p>
     
    </article>
  </div>
</div>	  
	  <p>Quality assurance (QA) is a way of preventing mistakes or defects in products and services and avoiding problems when delivering these solutions or services to customers; which ISO 9000 defines as "part of quality management focused on providing confidence that quality requirements will be fulfilled. Quality assurance is needed to enable success. It is needed to attain the excellence required to go to that new, next level of management.
	    <a id="displayText1" href="javascript:toggle1();">Know more..</a>
      <div id="toggleText1" style="display: none">
	  <p>At BerryCo, we love to manage product quality right from its source and all the way to the shelf. By engaging ourselves in quality testing, we help you to increase the value of your coffee with our quality assurance services. With quality assurance for a product or service, it is always easier to gain access to new markets and establish credibility in the same.  Our quality assurance services are rather all encompassing and include retailers and branding agencies, agents, etc.</p>
	  <p>We identify your quality issues and provide you with a detailed feedback on qualities such as:</p>
      <p>•	Moisture </p>
	  <p>•	grade </p>
	  <p>•	bulk density</p>
	  <p>Need your coffee analyzed? We can cup your coffee and provide a detailed analysis and report to bring out the coffee’s full potential. In this report we provide possible improvements to be implemented at the roasting level and/or green level. Also included, is the color measurement of whole bean and ground.</p>
	  <h2>Green Coffee Analysis</h2>
	  <p>Analysis of green coffee includes a detailed report on the green coffee grading, cupping score, ﬂavor proﬁle as well as possible the roast proﬁles and brew methods.</p>
	  <p>The Complete report of green bean grading will include but would not be limited to:</p>
	<p>•	Grading</p>
	  <p>•	Moisture</p>
	  <p>•	Density</p>
	  <p>•	Cupping notes</p>
	  <p>•	Roast Profile Recommendations</p>
	  <p>•	Brew Method Recommendations</p>
	  <h2>Roasted Coffee Analysis</h2>
	  <p>A full report on the roasted coffee would include</p>
	  <p>•	Cupping notes</p>
	  <p>•	Cupping score</p>
	  <p>•	Flavor Profile</p>
	  <p>•	Score</p>
		<p>•	Agtron reading of whole bean and ground coffee; and</p>
	  <p>•	Recommendations to improve</p>
	  </div>
	  </div>
	  </br>
	  </br>
	  
	  <div id="roaster">
	   <br/>
<br/>	 
	 	  <div class="wrapper row100 bgded" style="background-image:url('/website/images/demo/coffee3.jpg');">
  <div class="overlay">
    <article id="shout" class="capitalise clear"> 
    
     <center> <h2 class="heading"><font size="20" >For Roasters</font> </h2></center>
      <p><strong><a href="#"></a></strong></p>
    
    </article>
  </div>
</div>
	
	  <p>Roasting coffee transforms the chemical and physical properties of green coffee beans into roasted coffee products. The roasting process is what produces the characteristic flavor of coffee by causing the green coffee beans to change in taste.</p>
	  <p>At BerryCo we help the Roaster put his/her roasting business on the consumer market. To achieve this, we need to ensure that the roastery meets the consumer expectations and hence we help you with:</p>
	  <p>•	Inspection for green bean purchasing</p>
	  <p>•	selection of right equipment for your roastery </p>
	  <p>•	assisting in roasting skills 
	   <a id="displayText2" href="javascript:toggle2();">Know more..</a></p>
      <div id="toggleText2" style="display: none">
	  <p>•	suggestion of the best grinding and brewing techniques</p>
	  <p>•	analysing your current roasting unit/system </p>
	  <p>•	improving the manufacturing system</p>
	  <p>•	evaluation of roasted coffee and detailed feedback  on the same</p>
	  <p>•	flavor profiling and suggestion on the same</p>
	  <p>•	production facility management assistance in the areas of storage and packaging </p>
	  <h2>Roast Profile Development</h2>
	  <p>Blend development/product development according to the market. we help you to create roast profiles that will resonate with your brand and your customers and get the most from your coffee </p>
	  <p>How we help you -</p>
	  <p>•	Procurement of green coffee</p>
	  <p>•	Description of individual components</p>
	  <p>•	Three optional blends to choose from</p>
	  <p>•	Recommend brewing parameters</p>
	  <p>•	Final recipe for espresso blend</p>
	  <p>•	Roast Proﬁle Development</p>
	  <p>Create roast proﬁle per client’s roaster and speciﬁcations for taste proﬁle. Provide a detailed proﬁle as per client’s preferred documentation.</p>
	  <p>Features</p>
	  <p>Roast proﬁles developed based on client’s objectives and roasting machine </p>

	  
	  </div>
	  <div id="training">
	  <br/>
<br/>	 
	 	  <div class="wrapper row100 bgded" style="background-image:url('/website/images/demo/coffee4.jpg');">
  <div class="overlay">
    <article id="shout" class="capitalise clear"> 
    
      <center><h2 class="heading"><font size="20" >Training and Barista Skills</font> </h2> </center>
      <p><strong><a href="#"></a></strong></p>
   
    </article>
  </div>
</div>
	 
	  <p>Planning or implementing any espresso related venture? In the food service industry and especially in the beverages industry, the right kind of menu is your most important marketing tool. Your menu is what sets you apart from others. At BerryCo, we provide you the perfect solutions for that perfect menu and that perfect brew for the ever so perfect you!</p>
	  <p>Espresso is both a coffee beverage and a brewing method. It is not a specific bean, bean blend, or roast level. Any bean or roasting level can be used to produce authentic espresso, but it  needs sufficient expertise develop a good espresso and this is exactly where we come into the  picture.
	  <a id="displayText3" href="javascript:toggle3();">Know more..</a></p>
      <div id="toggleText3" style="display: none">
	  <p> At BerryCo we will help you with:</p>
	  <p>•	Developing the best espresso based drinks</p>
	  <p>•  Training in the preparation of all menu drinks</p>
	  <p>•	Training your employees in doing a fast and efficient job when coffee is in question.</p>
	  <p>•	Learning the technical skills of evaluation involved in preparation of drinks.</p>
	  <p>•	Training the barista to work in a café or interest to learn</p>
	  <p>•	How to prepare the best coffees in cafés</p>
	  <p>•	Preparation of recipes suited for your business</p>
	  <p>•	Servicing  and maintenance of equipments</p>
	  <p>•	 Interior decoration of café</p>
	  <p>•	Blend development for different target customer or market</p>
	  <p>•	Hands on training</p>
	  </div>
	  </div>
	  <div id="Business">
	   <br/>
<br/>	 
	  	 	  <div class="wrapper row100 bgded" style="background-image:url('/website/images/demo/coffee5.jpg');">
  <div class="overlay">
    <article id="shout" class="capitalise clear"> 
    
      <center><h2 class="heading"><font size="20" >Entrepreneurship  and <br/>Business Development</font> </h2></center>
      <p><strong><a href="#"></a></strong></p>
     
    </article>
  </div>
</div>
	 
	  <p>Starting your own business is undoubtedly stressful and pretty much demands your complete focus. However, it is also a very fulfilling experience, professionally and personally. We for one can vehemently vouch for it! As a business enterprise our self, we completely understand your needs and concerns more than anybody else. One of our favorite quotes is by LinkedIn Co-founder; Reid Hoffman who says “All humans are entrepreneurs not because they should start companies but because the 	 will to create is encoded in the human DNA!”</p>
	  <p>As part of our services offered under Entrepreneurship and Business Development, BerryCo aims to help you with
	  <a id="displayText4" href="javascript:toggle4();">Know more..</a></p>
      <div id="toggleText4" style="display: none"></p>
	  <p> Establishment of café</p>
<p>Depending on your needs and requirement, we will suggest you the kind of café that is likely to be the best for you. From small kiosks to larger cafes, we have different models of cafes available.</p>
<p>We will also help you understand the market segmentation in order to make your business work most effectively. </p>
<p>• Cost estimation, decoration, design, everything, prepare a project of different cost (budget) according to value of project, suggestions</p>
<p>• Purchasing, branding, registration, retailing, roasting unit</p>
<p>• Architect, modular project according to requirements and cost</p>
<p>• Suggestion of machinery </p>
<p>•	Establishment of Cafe</p>
<p>We will help you to create a sustainable business model by assisting you with a complete and detailed list of all supplies that you will need to start your business</p>
<p>a.	Purchase of equipments:  Advice on all the equipments that will be needed to operate your café</p>
<p>b.	Cost Analysis: Advice on the suppliers available to you near your location who will do the best job and reduce your operational costs.</p>
<p>c.	Brand Assistance: We will help you in creating a strong brand name for your product.</p>
<p>d.	Development and upliftment of existing cafés: How we do this – </p>
<p>•	diagnosing the problems and devising a plan to counter these problems</p>
<p>•	assessment on how your coffee shop currently performs</p>
<p>•	help you in establishing standard operating procedures and practices</p>
<p>•	identify target areas for improvement</p>
<p><b> Espresso Blend Development</b></p>	  
<p>•	Procurement of green coffee</p>	  
<p>•	Description of individual components</p>
<p>•	Three optional blends to choose from</p>
<p>•	Recommended brewing parameters</p>
<p>•	Final recipe for espresso blend</p>
  </div>
	  </div>
	  
	  
	  </font>

	  </main>
	  </div>

<div class="wrapper row4">
  <footer id="footer" class="clear"> 
 
    <div class="one_quarter first">
      <h6 class="title">Company Details</h6>
      <address class="btmspace-15">
      #1, Bhadrappa Layout,1st Main,1st cross, <br>
	 Kodigehalli Main Road,<br>
	 Near Big Bazar<br>
     Bangalore-560094<br/>
	 India
      </address>
      <ul class="nospace">
        <li class="btmspace-10"><span class="fa fa-phone"></span>+91 (886) 122 1284</li>
		<li class="btmspace-10"><span class="fa fa-phone"></span>+91 (966) 373 0974</li>
        <li><span class="fa fa-envelope-o"></span> mail@berryco.info</li>
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="title">From The Blog</h6>
      <article>
        <h2 class="nospace font-x1"><a href="#">CUP OF JOY</a></h2>
        <time class="smallfont" datetime="2045-04-06">Monday, 23<sup>rd</sup> November 2015</time>
        <p>Someone has very rightly said…Coffee is nothing but a hug in a mug. A cup of coffee shared with a friend or a loved one is always happiness shared and time very well spent... </p>
      </article>
    </div>
    
	<div class="one_quarter">
      <h6 class="title">Our Belief</h6>
      <p>We believe that qualitity is the only undying method of building trust and retaining customer. </p>
      <p>When you change the was look at things, the things you look at will change-Wayne Dyer </p>
    </div>
	
	
    <div class="one_quarter">
      <h6 class="title">Our Service</h6>
      <p>We at BerryCo believe coffee is more then just a drink and would rediscover the value around your coffee. </p>
      <p>Drown your troubles in us and we develop strategic solutions and guidance on the path to success.</p>
    </div>
 
  </footer>
</div>

<div class="wrapper">
  <div id="socialblock" class="clear"> 
 
    <ul class="faico clear">
      <li><a class="faicon-facebook" href="https://www.facebook.com/BerryCo-506568626176433/"><i class="fa fa-facebook"></i></a></li>
      <li><a class="faicon-twitter" href="https://twitter.com/BerryCocoffee"><i class="fa fa-twitter"></i></a></li>
      <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
      <li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
      <li><a class="faicon-rss" href="https://www.addtoany.com/share?linkurl=www.berryco.info&amp;linkname="><i class="fa fa-share-alt"></i></a></li>
<!-- AddToAny BEGIN -->
<script type="text/javascript">
var a2a_config = a2a_config || {};
a2a_config.linkurl = "www.berryco.info";
a2a_config.onclick = 1;
</script>
<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->

    </ul>
 
  </div>
</div>

<div class="wrapper row5">
  <div id="copyright" class="clear"> 
 
    <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="#">BerryCo.info</a></p>
   
  </div>
</div>

<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a> 
<!-- JAVASCRIPTS --> 

	
<script src="/website/layout/scripts/jquery.min.js"></script> 
<script src="/website/layout/scripts/jquery.backtotop.js"></script> 
<script src="/website/layout/scripts/jquery.mobilemenu.js"></script> 
<!-- IE9 Placeholder Support --> 
<script src="/website/layout/scripts/jquery.placeholder.min.js"></script> 

<!-- / IE9 Placeholder Support -->
</body>
</html>