<html>
<head>
<title>BerryCo-We Brew Solutions, Not Just Coffee.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="/website/css/bootstrap.min.css">
    <link rel="stylesheet" href="/website/layout/styles/fontawesome-4.3.0.min.css">
    <link rel="stylesheet" href="/website/css/form-elements.css">
        <link rel="stylesheet" href="/website/style.css">

    
<link rel="apple-touch-icon" sizes="180x180" href="/website/fevicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/website/fevicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/website/fevicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/website/fevicon/manifest.json">
<link rel="mask-icon" href="/website/fevicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/website/fevicon/favicon.ico">
<meta name="msapplication-config" content="/website/fevicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<link href="/website/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">


<style type="text/css">
	#float-bar {
	color:#555;
	width:50%;
	background-color:#a6ca8a;
    border-radius:10px;
    font-family:Tahoma,Geneva,Arial,sans-serif;font-size:11px;
    padding:10px 10px 10px 0px;
    margin:-40px 80px 15px 200px;
    border:1px solid #a6ca8a;
    text-align: center;
}
 
#float-bar a {color:#000;}
#float-bar a:hover {color:#000;text-decoration:none;}
 
#float-bar .notifyTextbld {display:block;margin-bottom:20px;}
.item-hover button:last-child
		{
			margin-top:5px !important;
		}
		@media(max-width:991px)
		{
			.item-hover button:last-child
			{
				margin-top:0px;
			}
		}
		.modal-dialog .modal-content .product_description p
		{
				color: #000;
			font-size: 19px;
			line-height: 27px;
		}
		.modal-dialog .modal-content .product_description .btn
		{
			border-radius:0px;
			border-color:#BE9145;
			height:45px;
		}
		.modal-dialog .modal-content .product_description .btn:hover,
		.modal-dialog .modal-content .product_description .btn:active,
		.modal-dialog .modal-content .product_description .btn:focus,
		.modal-dialog .modal-content .product_description .btn:visited
		{
			color :#fff !important;
		}
		.modal-dialog .modal-content .action_btn_grp
		{
			display:inline-block;
			margin:20px 0px;
			text-align:center;
		
		}
		.modal-dialog .modal-content .action_btn_grp .btn
		{
			margin-top:10px;
		}
		.modal-dialog .modal-content .action_btn_grp .btn
		{
			border-radius:0px;
			height:45px;
		}
		.modal-dialog .modal-content .modal-header
		{
			border-bottom:none;
		}
		.modal-dialog .modal-content .product_description
		{
			margin-top:50px;
		}
</style>


</head>

<body id="top">

<!-- Top Background Image Wrapper -->
<div class="row100 bgded" style="background-image:url('/website/images/demo/backgrounds/01.png');">
  <div class="overlay"> 
    
    <div class="wrapper">
      <header id="header" class="clear"> 
        
        @if(Session::get('success_msg'))
        <div id='float-bar' style=''>
				Product Added to Cart Successfully 
		</div>
		{{Session::forget('success_msg');}}
		@endif
		
        <div id="logo" class="fl_left">
         <h1><a href="{{URL::Route('WebIndex')}}"><img src="/website/images/demo/logo1.png" alt="logo"</a></h1>
        </div>
		
		 
       <div id="qct" class="fl_right">
         <?php if(!Auth::user()->check()) { ?>
          <ul class="nospace inline pushright">
            <li><a class="btn bold " href="{{URL::Route('WebLogin')}}" >Login</a></li> 
			
            <li><a class="btn bold" href="{{URL::Route('WebSignup')}}">Sign up</a></li>
          </ul>
          <?php }else{ ?>
          <ul class="nospace inline pushright">
            <li><i class=""></i><span> Welcome </span></li> &nbsp;
            <li><i class=""></i> <a href="">{{get_name(Auth::user()->id())}}</a></li>
          	<li><a class="" href="{{URL::Route('WebLogout')}}">Logout</a></li>
          </ul>
          <?php } ?>
        </div>
        
      </header>
    </div>
    
    <div class="wrapper padbot">
      <nav id="mainav">
        <ul class="clear">
         <li><a href="{{URL::Route('WebIndex')}}">Home</a></li>
          <li><a href="{{URL::Route('WebAbout')}}">About Us</a></li>
		  <li class="active"><a  href="{{URL::Route('WebEqu')}}">Coffee Brewers<sup style="color:red;font-size:8pt;">New</sup>	</a>
           
          </li>
          <li><a class="drop" href="{{URL::Route('WebService')}}">&nbsp;&nbsp;Services</a>
            <ul>
              <li><a href="{{URL::Route('WebService')}}#at_estate">At Estate</a></li>
              
              <li><a href="{{URL::Route('WebService')}}#qulality">Quality Assurance</a></li>
              <li><a href="{{URL::Route('WebService')}}#roaster">For Roasters</a></li>
			  <li><a href="{{URL::Route('WebService')}}#training">Barista and Training</a></li>
			  <li><a href="{{URL::Route('WebService')}}#Business">Business Development and entrepreneurship</a></li>
            </ul>
          </li>
          <li><a href="{{URL::Route('WebBlog')}}">&nbsp;&nbsp;Blog</a></li>
		      <li><a href="{{URL::Route('WebFaq')}}">&nbsp;&nbsp;FAQ</a></li>
              
              <li><a href="{{URL::Route('WebWeare')}}">&nbsp;&nbsp;Who we are?</a></li>
              
			
		  <li><a href="{{URL::Route('WebQur')}}">&nbsp;&nbsp;Queries?</a></li> 
        </ul>
      </nav>
    </div>
    
  </div>
</div>
<!-- End Top Background Image Wrapper --> 

<div class="wrapper row2">
  <div id="breadcrumb" class="clear"> 
   
    
      <li><a href="{{URL::Route('WebIndex')}}">Home</a></li>
     
      <li><a href="{{URL::Route('WebEqu')}}">Equipments</a></li>
      <form method="get" action="{{URL::Route('WebSession')}}" >
      	<input id="checkout" name="array1" value="{{$cart}}" type="hidden">
     
      
      <button  style="float:right;margin-top:-20px;width:10%;" class="btn-sm btn-danger btn-block" role="button">Checkout</button>
	  <button style="float:right;margin-top:-20px;margin-right:20px;width:10%;" class="btn-sm btn-success btn-block">Cart(<span id="main_cart">{{$count}}</span>)</button>
      </form>
    
    
  </div>
</div>

<div class="wrapper row3">
		<main class="clear">
			<div class="content"> 
				<div id="gallery">
					<figure>
						<header class="heading text-center">Artisan coffee Makers</header>
						<!-- Wrapper -->
						@foreach($brewers as $brewer)
						<div class="wrapper row100 bgded" style="background-image:url({{$brewer->banner_img}}); width:85%;margin-left:7%">
							<div class="overlay">
								<article id="shout" class="capitalise clear">
									<center><h2 class="heading"><font size="20" >{{$brewer->heading}}</font> </h2></center>
									<p><strong><a href="#"></a></strong></p>
								</article>
							</div>
						</div>
						<div class="container container-item">
							<!-- Item Outer -->
							<div class="item-outer">
								<div class="row">
						@foreach($items as $item)
							@if($item->brewer_id == $brewer->id)
									<div class="col-md-3">
										<!-- Item -->
										<div class="item" style="">
											<!-- Image -->
											<img src="{{$item->image}}" alt="" style="width:500px;height:225px" />
											<!-- Item Details -->	
											<div class="item-details">
												<h6 class="item-heading text-center" style="">{{$item->name}}</h6>
												<span style="">Rs {{get_number_format($item->price,2)}}</span>
											</div>


											<!-- Item Hover -->
											<div class="item-hover text-center" style="">
											<!-- <div class="" style=""> -->
											<button class="btn-sm btn-warning" style="margin-top:-215px;margin-left:100px;margin-bottom: 185px;" data-toggle="modal" onclick="popup(\'' .$item->name . '\',\'' .$item->price . '\',\'' .$item->image. '\');" data-target=".bs-example-modal-lg">Quick View</button>
												<a href="{{URL::Route('WebBuyAddCart',$item->id)}}"><button class="btn-sm btn-danger" style="width:30%;margin-top:0px;margin-left:0px;margin-bottom: -35px;"  role="button">Buy</button></a>
												<button class=" btn-sm btn-success" style="width:50%;margin-top:0px;margin-bottom: 150px;margin-left:90px;" onClick="add_cart({{$item->id}});" >Add to Cart</button>
																</div>

										</div>
									</div>
								@endif		
						@endforeach
								</div>
							</div>
						</div>

						@endforeach
					</figure>
				</div>
				
			</div>
			<div class="clear"></div>
		</main>
	</div>


<div class="wrapper row4">
  <footer id="footer" class="clear"> 
    
    <div class="one_quarter first">
      <h6 class="title">Company Details</h6>
      <address class="btmspace-15">
      #1, Bhadrappa Layout,1st Main,1st cross, <br>
	 Kodigehalli Main Road,<br>
	 Near Big Bazar<br>
     Bangalore-560094<br/>
	 India
      </address>
      <ul class="nospace">
        <li class="btmspace-10"><span class="fa fa-phone"></span>+91 (886) 122 1284</li>
		<li class="btmspace-10"><span class="fa fa-phone"></span>+91 (966) 373 0974</li>
        <li><span class="fa fa-envelope-o"></span> mail@berryco.info</li>
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="title">From The Blog</h6>
      <article>
        <h2 class="nospace font-x1"><a href="#">CUP OF JOY</a></h2>
        <time class="smallfont" datetime="2045-04-06">Monday, 23<sup>rd</sup> November 2015</time>
        <p>Someone has very rightly said…Coffee is nothing but a hug in a mug. A cup of coffee shared with a friend or a loved one is always happiness shared and time very well spent... </p>
      </article>
    </div>
    
	<div class="one_quarter">
      <h6 class="title">Our Believe</h6>
      <p>We believe that qualitity is the only undying method of building trust and retaining customer. </p>
      <p>When you change the was look at things, the things you look at will change-Wayne Dyer </p>
    </div>
	
	
    <div class="one_quarter">
      <h6 class="title">Our Service</h6>
      <p>We at BerryCo believe coffee is more then just a drink and would rediscover the value around your coffee. </p>
      <p>Drown your troubles in us and we develop strategic solutions and guidance on the path to success.</p>
    </div>
    
  </footer>
</div>

<div class="wrapper">
  <div id="socialblock" class="clear"> 
    
    <ul class="faico clear">
      <li><a class="faicon-facebook" href="https://www.facebook.com/BerryCo-506568626176433/"><i class="fa fa-facebook"></i></a></li>
      <li><a class="faicon-twitter" href="https://twitter.com/BerryCocoffee"><i class="fa fa-twitter"></i></a></li>
      <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
      <li><a class="faicon-google-plus" href=""><i class="fa fa-google-plus"></i></a></li>
      <li><a class="faicon-rss" href=""><i class="fa fa-rss"></i></a></li>

	</ul>
    
  </div>
</div>

<div class="wrapper row5">
  <div id="copyright" class="clear"> 
   
    <p class="fl_left">Copyright &copy; 2015 - All Rights Reserved - <a href="#">Domain Name</a></p>
    <p class="fl_right">Template by <a target="_blank" href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
    
  </div>
</div>


<!-- model start -->
	<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	  <div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		 <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			
		  </div>
		   <div class="modal-body">
			<div class="row">
				<div class="col-md-7">
					<div class="image-outer" >
						<img src="img/a1.jpg" id="img_pop" class="img-responsive" width="100%"/> 
					</div>
				</div>
				<div class="col-md-5">
					<div  class="product_description">
						<p id="title_pop" >
							AeroPress Coffee & Espresso Maker with ToTe bag
						</p>
						<p id="price_pop" class="price-tag">
							Rs 2,640.00
						</p>
					</div>
					<span class="action_btn_grp">
						<button type="button" class="btn btn-danger">ADD TO CART</button>
						<button type="button" class="btn btn-warning">LEARN MORE</button>
					</span>
				</div>
			</div>
		  </div>
		</div>
	  </div>
	</div>

<!-- model end -->

<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a> 
<!-- JAVASCRIPTS --> 
<script src="/website/layout/scripts/jquery.min.js"></script> 
<script src="/website/layout/scripts/jquery.backtotop.js"></script> 
<script src="/website/layout/scripts/jquery.mobilemenu.js"></script> 
<!-- IE9 Placeholder Support --> 
<script src="/website/layout/scripts/jquery.placeholder.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
					function div_show() {
					document.getElementById('abc').style.display = "block";
					}
					//Function to Hide Popup
					function div_hide(){
					document.getElementById('abc').style.display = "none";
					}
</script>
<script src="/website/js/jquery.min.js"></script> 
	<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="/website/js/jquery.backtotop.js"></script> 
	<script src="/website/js/jquery.mobilemenu.js"></script> 
	<!-- IE9 Placeholder Support --> 
	<script src="/website/js/jquery.placeholder.min.js"></script>
	<script>
		function div_show() {
		document.getElementById('abc').style.display = "block";
		}
		//Function to Hide Popup
		function div_hide(){
		document.getElementById('abc').style.display = "none";
		}
		
		//Hover Effect 
		$(document).ready(function(){
			$(".item-hover").hide();
			$(".item").hover(function(){
				if(!($(this).hasClass("active"))){
					$(this).children(".item-hover").fadeIn();
					$(this).addClass("active");
				}
				else{
					$(this).children(".item-hover").fadeOut();
					$(this).removeClass("active");
				}
				
			});
		});
	</script>

<script type="text/javascript">
	var cart = new Array();
	
	var k = document.getElementById('array1').value;	
	function add_cart(id){
	// 	console.log(k);
	// if(cart.indexOf(id) == -1)
	// {
	// 	cart.push(id);
	// }
	// 	document.getElementById('main_cart').innerHTML=cart.length;
	// 	document.getElementById('checkout').value=cart;

    var dataString = 'id='+id; 
    $.ajax({
    type: "GET",
    url : 'add_cart',
    data : dataString,
    success : function(data){
    	 window.location.href = "/equipment";
    }
    });
}

</script>

<script type="text/javascript">

function popup(name,price,img){
	document.getElementById("title_pop").innerHTML = name ;
	document.getElementById("price_pop").innerHTML = price ;
	document.getElementById("img_pop").innerHTML = img ;
}	

</script>

<script type='text/javascript'>
$(document).ready(function(){
$('#float-bar').show(function(){
setTimeout(function (){
$('#float-bar').slideUp(445);
},1000);
});
});
</script>


</body>
</html>