<!DOCTYPE html>
<html>
<head>
<title>BerryCo-We Brew Solutions, Not Just Coffee.</title>
<meta charset="utf-8">
<meta name="viewport"  content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="/website/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">

<!-- Top Background Image Wrapper -->
<div class="row100 bgded" style="background-image:url('/website/images/demo/backgrounds/01.png');">
  <div class="overlay"> 
    
    <div class="wrapper">
      <header id="header" class="clear"> 
       
        <div id="logo" class="fl_left">
         <h1><a href="{{URL::Route('WebIndex')}}"><img src="/website/images/demo/logo1.png" alt="logo"</a></h1>
        </div>
		
		 
        <div id="qct" class="fl_right">
         <?php if(!Auth::user()->check()) { ?>
          <ul class="nospace inline pushright">
            <li><a class="btn bold" href="{{URL::Route('WebLogin')}}" >Login</a></li> 
      
            <li><a class="btn bold" href="{{URL::Route('WebSignup')}}">Sign up</a></li>
          </ul>
          <?php }else{ ?>
          <ul class="nospace inline pushright">
            <li><i class=""></i><span> Welcome </span></li> &nbsp;
            <li><i class=""></i> <a href="">{{get_name(Auth::user()->id())}}</a></li>
            <li><a class="" href="{{URL::Route('WebLogout')}}">Logout</a></li>
          </ul>
          <?php } ?>
        </div>
        
      </header>
    </div>
    
    <div class="wrapper padbot">
      <nav id="mainav">
        <ul class="clear">
         <li><a href="{{URL::Route('WebIndex')}}">&nbsp;&nbsp;Home</a></li>
          <li><a href="{{URL::Route('WebAbout')}}">&nbsp;&nbsp;About Us</a></li>
		   <li><a  href="{{URL::Route('WebEqu')}}">Coffee Brewers<sup style="color:red;font-size:7pt;">New</sup>	</a>
           
          </li>
          <li><a class="drop" href="{{URL::Route('WebService')}}">&nbsp;&nbsp;Services</a>
            <ul>
              <li><a href="{{URL::Route('WebService')}}#at_estate">At Estate</a></li>
              
              <li><a href="{{URL::Route('WebService')}}#qulality">Quality Assurance</a></li>
              <li><a href="{{URL::Route('WebService')}}#roaster">For Roasters</a></li>
			  <li><a href="{{URL::Route('WebService')}}#training">Barista and Training</a></li>
			  <li><a href="{{URL::Route('WebService')}}#Business">Business Development and entrepreneurship</a></li>
            </ul>
          </li>
          <li><a href="{{URL::Route('WebBlog')}}">&nbsp;&nbsp;Blog</a></li>
		      <li><a href="{{URL::Route('WebFaq')}}">&nbsp;&nbsp;FAQ</a></li>
              
              <li><a href="{{URL::Route('WebWeare')}}">&nbsp;&nbsp;Who we are?</a></li>
              
			
		  <li class="active"><a href="{{URL::Route('WebQur')}}">&nbsp;&nbsp;Queries?</a></li> 
        </ul>
      </nav>
    </div>
   
  </div>
</div>
<!-- End Top Background Image Wrapper --> 

<div class="wrapper row2">
  <div id="breadcrumb" class="clear"> 
    
    <ul>
      <li><a href="{{URL::Route('WebIndex')}}">Home</a></li>
     
      <li><a href="{{URL::Route('WebQur')}}">Queries</a></li>
    </ul>
    
  </div>
</div>

<div class="wrapper row3">
  <main class="container clear"> 
    <!-- main body --> 
   
    <div class="content three_quarter first"> 
     
 
      <div id="comments">
        
        
        <h2>To Know Berryco right companion for your need , get in touch with us at</h2>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div style="float: right;"> <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="350" height="300" src="https://maps.google.com/maps?hl=en&q=BerryCo&ie=UTF8&t=roadmap&z=16&iwloc=B&output=embed"><div><small><a href="http://embedgooglemaps.com">berryco.info</a></small></div><div><small><a href="http://berryco.info/">berryco.info</a></small></div></iframe></div>		
		<ul class="nospace inline pushright">
            <li><i class="fa fa-phone"></i>  +91 (886) 122 1284</li>  </br>
			<li><i class="fa fa-phone"></i>  +91 (966) 373 0974</li>  </br>
			
            <li><i class="fa fa-envelope-o"></i>  info@berryco.coffee</li></br>
          
		 <li><i class="fa fa-home"></i>BerryCo(I) Pvt. Ltd.<br>
	 #1, Bhadrappa Layout,1st Main,1st cross, <br>
	 Kodigehalli Main Road,<br>
	 Near Big Bazar<br>
     Bangalore-560094.<br>
      India
      </li>
	  </ul>
	  
	  <h2> Further Queries ? </h2>
        <form action="../php/send_email.php" method="post">
          <div class="one_half first">
            <label for="name">Name <span>*</span></label>
            <input type="text" name="name" id="name" value="" size="22" required>
          </div>
		  <div class="one_half second">
            <label for="c_name">Company Name</label>
            <input type="text" name="c_name" id="name" value="" size="22">
          </div>
		  <div class="one_half first">
            <label for="contact">Contact No <span>*</span></label>
            <input type="text" name="contact" id="name" value="" size="22" required>
          </div>
          <div class="one_half second">
            <label for="email">E-Mail <span>*</span></label>
            <input type="text" name="email" id="email" value="" size="22" required>
          </div>
          
          <div class="block clear">
            <label for="comment">How can we help you?<span>*</span></label>
            <textarea name="comment" id="comment" cols="25" rows="10" required></textarea>
          </div>
          <div>
            <input name="submit" type="submit" value="Submit Form">
            &nbsp;
            <input name="reset" type="reset" value="Reset Form">
          </div>
        </form>
      </div>
	  <span>*</span> Visits by appointment. Please give us a call so we would know you are coming to help you better with a cup of coffee and more.</br>
	  
	  </br>
	  Thank you for your interest in BerryCo.
      
    </div>
    
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>



<div class="wrapper row4">
  <footer id="footer" class="clear"> 
   
    <div class="one_quarter first">
      <h6 class="title">Company Details</h6>
      <address class="btmspace-15">
      #1, Bhadrappa Layout,1st Main,1st cross, <br>
	 Kodigehalli Main Road,<br>
	 Near Big Bazar<br>
     Bangalore-560094<br/>
	 India
      </address>
      <ul class="nospace">
        <li class="btmspace-10"><span class="fa fa-phone"></span>+91 (886) 122 1284</li>
		<li class="btmspace-10"><span class="fa fa-phone"></span>+91 (966) 373 0974</li>
        <li><span class="fa fa-envelope-o"></span> mail@berryco.info</li>
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="title">From The Blog</h6>
      <article>
        <h2 class="nospace font-x1"><a href="#">CUP OF JOY</a></h2>
        <time class="smallfont" datetime="2045-04-06">Monday, 23<sup>rd</sup> November 2015</time>
        <p>Someone has very rightly said…Coffee is nothing but a hug in a mug. A cup of coffee shared with a friend or a loved one is always happiness shared and time very well spent... </p>
      </article>
    </div>
    
	<div class="one_quarter">
      <h6 class="title">Our Belief</h6>
      <p>We believe that qualitity is the only undying method of building trust and retaining customer. </p>
      <p>When you change the was look at things, the things you look at will change-Wayne Dyer </p>
    </div>
	
	
    <div class="one_quarter">
      <h6 class="title">Our Service</h6>
      <p>We at BerryCo believe coffee is more then just a drink and would rediscover the value around your coffee. </p>
      <p>Drown your troubles in us and we develop strategic solutions and guidance on the path to success.</p>
    </div>
    
  </footer>
</div>

</div>

<div class="wrapper">
  <div id="socialblock" class="clear"> 
        <ul class="faico clear">
      <li><a class="faicon-facebook" href="https://www.facebook.com/BerryCo-506568626176433/"><i class="fa fa-facebook"></i></a></li>
      <li><a class="faicon-twitter" href="https://twitter.com/BerryCocoffee"><i class="fa fa-twitter"></i></a></li>
      <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
      <li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
      <li><a class="faicon-rss" href="https://www.addtoany.com/share?linkurl=www.berryco.info&amp;linkname="><i class="fa fa-share-alt"></i></a></li>
<!-- AddToAny BEGIN -->
<script type="text/javascript">
var a2a_config = a2a_config || {};
a2a_config.linkurl = "www.berryco.info";
a2a_config.onclick = 1;
</script>
<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->

    </ul>
     
  </div>
</div>

<div class="wrapper row5">
  <div id="copyright" class="clear"> 
    
    <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="#">Berryco.info</a></p>
   
    
  </div>
</div>
 
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a> 
<!-- JAVASCRIPTS --> 
<script src="/website/layout/scripts/jquery.min.js"></script> 
<script src="/website/layout/scripts/jquery.backtotop.js"></script> 
<script src="/website/layout/scripts/jquery.mobilemenu.js"></script> 
<!-- IE9 Placeholder Support --> 
<script src="/website/layout/scripts/jquery.placeholder.min.js"></script> 
<!-- / IE9 Placeholder Support -->
</body>
</html>