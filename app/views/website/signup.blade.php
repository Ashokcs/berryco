<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Signup</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="/website/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="/website/css/form-elements.css">
        <link rel="stylesheet" href="/website/css/style.css">
        <link rel="shortcut icon" href="/website/fevicon/favicon.ico">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
 <style>
            .login-form.form-box{
                max-width: 600px;margin: 180px auto 100px;
            }
            .form-top{
                padding: 10px 10px;background:#BE9145
            }
            .form-top h3{
                color: #fff;font-size: 30px;padding: 0px 20px;font-weight: 400;
            }
            .form-top-right{
                line-height: 0px;
            }
            .form-bottom{
                text-align: center;
            }
            .login-form .btn{
                padding: 10px 0px;
                height: auto;
                width: 170px;
                font-size: 18px;
                font-weight: 400;
                line-height: 22px;
                margin: 20px auto;
            }
            p{
                font-size: 16px;font-weight: 400;
            }
        </style>
</head>
    <body style="background: url('/website/images/demo/backgrounds/03.jpg') no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
	<div class="outer">
        <!-- Top content -->
        <div class="top-content" style="">
            <div class="inner-bg">
                <div class="container">
                   
                    
                 <div class="row">
                        <div class="col-sm-12">
                            <div class="login-form form-box" style="">
                                <div class="form-top" style="">
                                    <div class="form-top-left">
                                        <h3 style="">Signup to our site</h3>
                                        <!-- <p>Enter username and password to log on:</p> -->
                                    </div>
                                    <div class="form-top-right">
                                       <a href="{{URL::Route('WebIndex')}}"> <img src="/website/images/demo/logo.png" style="width: 100%;"></a>
                                    </div>
                                </div>

                                <div class="form-bottom" style="">
                                    <a href="{{URL::Route('FBSignup')}}" class="btn btn-link-1 btn-link-1-facebook" style="">
                                        <i class="fa fa-facebook"></i> Facebook
                                    </a>
                                    <a href="{{URL::Route('GLSignup')}}" class="btn btn-link-1 btn-link-1-google-plus" style="" >
                                        <i class="fa fa-google-plus"></i> Google Plus
                                    </a>
                                     <p style="">You are existing User then<a href="{{URL::Route('WebLogin')}}"> click here </a>to login</p>
                                </div>
                            </div>
                        
                            <!-- <div class="social-login">
                                <h3 style="color: black;">You are new User then<a href=""> click here </a>to signup:</h3>
                                <div class="social-login-buttons">
                                    
                                </div>
                            </div> -->
                            
                        </div>
                       
                    </div>
                    
                </div>
            </div>
            
        </div>

        <!-- Footer -->
        <footer>
        		
        </footer>
	</div>

        <!-- Javascript -->
        <script src="websitejs/jquery-1.11.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="websitejs/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>