<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Signup</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="/website/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="/website/css/form-elements.css">
        <link rel="stylesheet" href="/website/css/style.css">
        <link rel="shortcut icon" href="/website/fevicon/favicon.ico">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
<style>
            .login-form.form-box{
                max-width: 600px;margin: 180px auto 100px;
            }
            .form-top{
                padding: 20px 20px;background:#986b1f
            }
            .form-top h3{
                color: #fff;font-size: 30px;padding: 0px 20px;font-weight: 400;
            }
            .form-top-right{
                line-height: 0px;
            }
            .form-bottom{
                text-align: center;
            }
            .login-form .btn{
                padding: 10px 0px;
                height: auto;
                width: 170px;
                font-size: 18px;
                font-weight: 400;
                line-height: 22px;
                margin: 20px auto;
            }
            p{
                font-size: 16px;font-weight: 400;
            }
        </style>
</head>
    <body style="background: url('/website/images/demo/backgrounds/03.jpg') no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
	<div class="outer">
        <!-- Top content -->
        <div class="top-content" style="">
            <div class="inner-bg">
                <div class="container">
                    
                    
                  
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="login-form form-box" style="margin: 15px auto;max-width:500px;">
                                <div class="form-top" style="">
                                    <div class="form-top-left">
                                        <h3 style="">Please Complete Your Signup process..</h3>
                                        <!-- <p>Enter username and password to log on:</p> -->
                                    </div>
                                    <div class="form-top-right">
                                        <a href="{{URL::Route('WebIndex')}}"> <img src="/website/images/demo/logo.png" style="width: 100%;"></a>
                                    </div>
                                </div>

                                <div class="form-bottom" style="">
                                   <form role="form" action="{{URL::Route('WebSaveSignup')}}" method="get" class="registration-form">
                                        
                                        <input type="hidden" name="social_unique_id" value="<?= $result['id']  ?>" >
                                        <input type="hidden" name="name" value="<?= $result['name']  ?>" >
                                        <input type="hidden" name="login_by" value="<?= $login_by  ?>" >
                                        <div class="form-group">
                                            <label class="sr-only" for="form-last-name">Phone</label>
                                            <input type="text" name="phone" placeholder="Phone..." class="form-last-name form-control" id="form-last-name" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="form-email">Email</label>
                                            <input type="text" name="email" placeholder="Email..." value="<?= $result['email'] ?>" class="form-email form-control" id="form-email" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="form-about-yourself">Address</label>
                                            <textarea name="address" placeholder="Enter Your Address" class="form-about-yourself form-control" required ></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="form-last-name">Pincode</label>
                                            <input type="text" name="pincode" placeholder="Pincode..." class="form-last-name form-control" id="form-last-name" required>
                                        </div>
                                        <button type="submit" style="background:#986b1f;" class="btn">Sign up</button>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                       
                    </div>                    
                </div>
            </div>
            
        </div>

        <!-- Footer -->
        <footer>
        		
        </footer>
	</div>

        <!-- Javascript -->
        <script src="websitejs/jquery-1.11.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="websitejs/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>