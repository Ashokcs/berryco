<!DOCTYPE html>

<html>
<head>
<title>BerryCo-We Brew Solutions, Not Just Coffee.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="/website/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<link rel="apple-touch-icon" sizes="180x180" href="/website/fevicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/website/fevicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/website/fevicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/website/fevicon/manifest.json">
<link rel="mask-icon" href="/website/fevicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/website/fevicon/favicon.ico">
<meta name="msapplication-config" content="/website/fevicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
</head>
<body id="top">

<!-- Top Background Image Wrapper -->
<div class="row100 bgded" style="background-image:url('/website/images/demo/backgrounds/01.png');">
  <div class="overlay"> 
  
   <div class="wrapper">
      <header id="header" class="clear"> 
   
        <div id="logo" class="fl_left">
         <h1><a href="{{URL::Route('WebIndex')}}"><img src="/website/images/demo/logo1.png" alt="logo"</a></h1>
        </div>
    
     
        <div id="qct" class="fl_right">
         <?php if(!Auth::user()->check()) { ?>
          <ul class="nospace inline pushright">
            <li><a class="btn bold" href="{{URL::Route('WebLogin')}}" >Login</a></li> 
      
            <li><a class="btn bold" href="{{URL::Route('WebSignup')}}">Sign up</a></li>
          </ul>
          <?php }else{ ?>
          <ul class="nospace inline pushright">
            <li><i class=""></i><span> Welcome </span></li> &nbsp;
            <li><i class=""></i> <a href="">{{get_name(Auth::user()->id())}}</a></li>
            <li><a class="" href="{{URL::Route('WebLogout')}}">Logout</a></li>
          </ul>
          <?php } ?>
        </div>
        
      </header>
    </div>
    
  

   <div class="wrapper padbot">
      <nav id="mainav">
        <ul class="clear">
          <li><a href="{{URL::Route('WebIndex')}}">&nbsp;Home&nbsp;</a></li>
          <li class="active" ><a href="{{URL::Route('WebAbout')}}">&nbsp;About Us&nbsp;</a></li>
    <li><a  href="{{URL::Route('WebEqu')}}">Coffee Brewers<sup style="color:red;font-size:7pt;">New</sup> </a>
           
          </li>
          <li><a class="drop" href="{{URL::Route('WebService')}}">&nbsp;Services&nbsp;</a>
            <ul>
              <li><a href="{{URL::Route('WebService')}}#at_estate">At Estate</a></li>
              
              <li><a href="{{URL::Route('WebService')}}#qulality">Quality Assurance</a></li>
              <li><a href="{{URL::Route('WebService')}}#roaster">For Roasters</a></li>
        <li><a href="{{URL::Route('WebService')}}#training">Barista and Training</a></li>
        <li><a href="{{URL::Route('WebService')}}#Business">Business Development and entrepreneurship</a></li>
            </ul>
          </li>
          <li><a href="{{URL::Route('WebBlog')}}">&nbsp;Blog&nbsp;</a></li>
          <li><a href="{{URL::Route('WebFaq')}}">&nbsp;FAQ&nbsp;</a></li>
              
              <li><a href="{{URL::Route('WebWeare')}}">&nbsp;Who we are?&nbsp;</a></li>
              
      
      <li><a href="{{URL::Route('WebQur')}}">&nbsp;Queries?&nbsp;</a></li>
      
        </ul>
      </nav>
    </div>
  
  </div>
</div>
<!-- End Top Background Image Wrapper --> 

<div class="wrapper row2">
  <div id="breadcrumb" class="clear"> 
  
    <ul>
      <li><a href="{{URL::Route('WebIndex')}}">Home</a></li>
      
      <li><a href="{{URL::Route('WebAbout')}}">About Us</a></li>
    </ul>
   
  </div>
</div>

<div class="wrapper row3">
  <main class="container clear"> 
    <!-- main body --> 
   
    <div class="content"> 
     
      <h1><b>About Us</b></h1>
      <img class="imgr borderedbox inspace-5" src="/website/images/demo/abo.jpg" alt="">
    
      <p>BerryCo coffee solutions believes that quality is not an act, it is a habit! We have thus made it our habit to 

serve our customers with quality par excellence. Our aim is to provide our customers with real service one that cannot be measured with money and is a legacy at its best..</p>
      <p>A customer is the most important visitor on our premises, he is not dependent on us. We are dependent on him. He is not an interruption in our work. He is the purpose of it. He is not an outsider in our business. He is part of it. We are not doing him a favor by serving him. He is doing us a favor by giving us an opportunity to do so. - Mahatma Gandhi</p>
      <img class="imgl borderedbox inspace-5" src="/website/images/demo/ans.jpg" alt="">
      <p>Every process and detail of our business is designed to deliver quality solutions for the final cup of quality coffee.</p>
      <p>As we belong to one of the top 10 producing countries we strongly believe in quality coffee, which today is the most important factor for effective coffee chain development. With this mission in mind we are determined to radiate new and improved methods for delivering high quality coffee to all sections of society. In our perpetual strive we aspire to make India and its coffee an international destination.</p>
      <p>We ensure that all our programs are custom designed according to the needs and requirements for each of our customer.</p>
      </main>
    </div>

<div class="wrapper row4">
  <footer id="footer" class="clear"> 
  
    <div class="one_quarter first">
      <h6 class="title">Company Details</h6>
      <address class="btmspace-15">
      #1, Bhadrappa Layout,1st Main,1st cross, <br>
   Kodigehalli Main Road,<br>
   Near Big Bazar<br>
     Bangalore-560094<br/>
   India
      </address>
      <ul class="nospace">
        <li class="btmspace-10"><span class="fa fa-phone"></span>+91 (886) 122 1284</li>
    <li class="btmspace-10"><span class="fa fa-phone"></span>+91 (966) 373 0974</li>
        <li><span class="fa fa-envelope-o"></span> mail@berryco.info</li>
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="title">From The Blog</h6>
      <article>
        <h2 class="nospace font-x1"><a href="#">CUP OF JOY</a></h2>
        <time class="smallfont" datetime="2045-04-06">Monday, 23<sup>rd</sup> November 2015</time>
        <p>Someone has very rightly said…Coffee is nothing but a hug in a mug. A cup of coffee shared with a friend or a loved one is always happiness shared and time very well spent... </p>
      </article>
    </div>
    
  <div class="one_quarter">
      <h6 class="title">Our Belief</h6>
      <p>We believe that qualitity is the only undying method of building trust and retaining customer. </p>
      <p>When you change the was look at things, the things you look at will change-Wayne Dyer </p>
    </div>
  
  
    <div class="one_quarter">
      <h6 class="title">Our Service</h6>
      <p>We at BerryCo believe coffee is more then just a drink and would rediscover the value around your coffee. </p>
      <p>Drown your troubles in us and we develop strategic solutions and guidance on the path to success.</p>
    </div>
  
  </footer>
</div>

<div class="wrapper">
  <div id="socialblock" class="clear"> 
 
    <ul class="faico clear">
      <li><a class="faicon-facebook" href="https://www.facebook.com/BerryCo-506568626176433/"><i class="fa fa-facebook"></i></a></li>
      <li><a class="faicon-twitter" href="https://twitter.com/BerryCocoffee"><i class="fa fa-twitter"></i></a></li>
      <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
      <li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
      <li><a class="faicon-rss" href="https://www.addtoany.com/share?linkurl=www.berryco.info&amp;linkname="><i class="fa fa-share-alt"></i></a></li>
<!-- AddToAny BEGIN -->
<script type="text/javascript">
var a2a_config = a2a_config || {};
a2a_config.linkurl = "www.berryco.info";
a2a_config.onclick = 1;
</script>
<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->
 </ul>
    
  </div>
</div>

<div class="wrapper row5">
  <div id="copyright" class="clear"> 
    <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="#">BerryCo.info</a></p>
   
  
  </div>
</div>

<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a> 
<!-- JAVASCRIPTS --> 
<script src="/website/layout/scripts/jquery.min.js"></script> 
<script src="/website/layout/scripts/jquery.backtotop.js"></script> 
<script src="/website/layout/scripts/jquery.mobilemenu.js"></script> 
<!-- IE9 Placeholder Support --> 
<script src="/website/layout/scripts/jquery.placeholder.min.js"></script> 
<!-- / IE9 Placeholder Support -->
</body>
</html>