<html>
  <head>
  <title>Payment Status</title>
  <!-- css -->
    <link href="/website/checkout/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/website/checkout/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <meta http-equiv="refresh" content="5;{{URL::Route('WebIndex');}}" />
    <!--<link href="css/default.css" rel="stylesheet" type="text/css">-->
    
    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
    
  
  </head>
  <body >
   
    <div class="checkout-outer">
        <div class="container">
         
          <div class="checkout-inner">
            <div class="row">
              <div class="">
                <div class="center_section">
                  <div class="panel panel-default" style="margin-top:20px;">
                    <div class="panel-heading">
                      <p>Payment Status</p>
                    </div>
                    <div class="panel-body">
                      <div class="payment-type">
                              <p style="color:green;font-weight: 300;font-size: 25px;" ><b>Payment Successful ...!</b></p>
                              
    <div class="red box">
                <b>Page will automatically redirect to home page....</b> &nbsp;&nbsp;
                <!-- <button value="Submit" class="btn btn-info">Home</button> -->
          </div>
    
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<script type='text/javascript'>
// $(document).ready(function(){
// setTimeout(function (){
//   window.location.href = "/equipment";
// },1000);
// });
</script>
</body>
</html>