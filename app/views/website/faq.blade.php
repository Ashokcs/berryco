<!DOCTYPE html>

<html>
<head>
<title>BerryCo-We Brew Solutions, Not Just Coffee.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="/website/layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	

<link rel="apple-touch-icon" sizes="180x180" href="/website/fevicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/website/fevicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/website/fevicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/website/fevicon/manifest.json">
<link rel="mask-icon" href="/website/fevicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/website/fevicon/favicon.ico">
<meta name="msapplication-config" content="/website/fevicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">	

	
	<meta name="robots" content="noindex,nofollow"/>
	<link media="all" type="text/css" rel="stylesheet" href="/website/style.css">
</head>
<body id="top">
<!-- Top Background Image Wrapper -->
<div class="row100 bgded" style="background-image:url('/website/images/demo/backgrounds/01.png');">
  <div class="overlay"> 
  
    <div class="wrapper">
      <header id="header" class="clear"> 
    
        <div id="logo" class="fl_left">
         <h1><a href="{{URL::Route('WebIndex')}}"><img src="/website/images/demo/logo1.png" alt="logo"</a></h1>
        </div>
		
		 
        <div id="qct" class="fl_right">
         <?php if(!Auth::user()->check()) { ?>
          <ul class="nospace inline pushright">
            <li><a class="btn bold" href="{{URL::Route('WebLogin')}}" >Login</a></li> 
			
            <li><a class="btn bold" href="{{URL::Route('WebSignup')}}">Sign up</a></li>
          </ul>
          <?php }else{ ?>
          <ul class="nospace inline pushright">
            <li><i class=""></i><span> Welcome </span></li> &nbsp;
            <li><i class=""></i> <a href="">{{get_name(Auth::user()->id())}}</a></li>
          	<li><a class="" href="{{URL::Route('WebLogout')}}">Logout</a></li>
          </ul>
          <?php } ?>
        </div>
      
      </header>
    </div>
	<br/>
  
   <div class="wrapper padbot">
      <nav id="mainav">
        <ul class="clear">
<li><a href="{{URL::Route('WebIndex')}}">&nbsp;Home&nbsp;</a></li>
          <li><a href="{{URL::Route('WebAbout')}}">&nbsp;About Us&nbsp;</a></li>
		   <li><a  href="{{URL::Route('WebEqu')}}">Coffee Brewers<sup style="color:red;font-size:7pt;">New</sup>	</a>
           
          </li>
          <li><a class="drop" href="{{URL::Route('WebService')}}">&nbsp;Services&nbsp;</a>
            <ul>
              <li><a href="{{URL::Route('WebService')}}#at_estate">At Estate</a></li>
              
              <li><a href="{{URL::Route('WebService')}}#quality">Quality Assurance</a></li>
              <li><a href="{{URL::Route('WebService')}}#roaster">For Roasters</a></li>
			  <li><a href="{{URL::Route('WebService')}}#training">Barista and Training</a></li>
			  <li><a href="{{URL::Route('WebService')}}#Business">Business Development and entrepreneurship</a></li>
            </ul>
          </li>
          <li><a href="{{URL::Route('WebBlog')}}">&nbsp;Blog&nbsp;</a></li>
		      <li class="active"><a href="{{URL::Route('WebFaq')}}">&nbsp;FAQ&nbsp;</a></li>
              
              <li><a href="{{URL::Route('WebWeare')}}">&nbsp;Who we are?&nbsp;</a></li>
              
			
		  <li><a href="{{URL::Route('WebQur')}}">&nbsp;Queries?&nbsp;</a></li>
        </ul>
      </nav>
    </div>
   
  </div>
</div>
<!-- End Top Background Image Wrapper --> 

<div class="wrapper row2">
  <div id="breadcrumb" class="clear"> 
   
    <ul>
      <li><a href="{{URL::Route('WebIndex')}}">Home</a></li>
      
      <li><a href="{{URL::Route('WebFaq')}}">FAQ</a></li>
    </ul>
    
  </div>
</div>

<div class="wrapper row3">
  <main class="container clear"> 
    <!-- main body --> 
    
    <div class="content"> 
     
	  <body>
		<div class="page-wrap">
	<div class="accordians-div">
		<div class="accordian-about-page">
			<div class="content ">
			
								   <div class="about-content">
								   	<h3>Frequently Asked Questions</h3>
								</div>

			</div>
			<span size="3.5">
			<ul class="about-ul">
								<li class="about-li">
					<input type="checkbox" class="input2" checked>
					<div class="about-accordian-head">
						<span class="add-button">
							<img src="/website/images/demo/plus.png" class="plus" />
							<img src="/website/images/demo/minus.png" class="minus" />
						</span>
						<span class="heading-span"><h3>What are coffee beans ?</h3></span>
						
					</div>
					<div class="about-accordian-content">
					<p>Coffee beans are seeds of a small berry .Each berry has two green coffee beans.When the beans are roasted ,they expand ,pop open to give the more familiar look .</p>
					<p>There are only two types of commercial coffee beans grown today (Arabica and Robusta)</p>
					</div>
				</li>
								<li class="about-li">
					<input type="checkbox" class="input2" checked>
					<div class="about-accordian-head">
						<span class="add-button">
							<img src="/website/images/demo/plus.png" class="plus" />
							<img src="/website/images/demo/minus.png" class="minus" />
						</span>
						<span class="heading-span"><h3>What are the different roast variations ? </h3></span>
						
					</div>
					<div class="about-accordian-content">
				
					<p> Cinammon roast/light roast </p>
					<p>New England roast</p>
					<p>Light American roast</p>
					<p>City roast</p>
					<p>French roast</p>
					<p>Spanish roast</p>
					
					</div>
				</li>
								<li class="about-li">
					<input type="checkbox" class="input2" checked>
					<div class="about-accordian-head">
						<span class="add-button">
							<img src="/website/images/demo/plus.png" class="plus" />
							<img src="/website/images/demo/minus.png" class="minus" />
						</span>
						<span class="heading-span"><h3>Which are the different countries producing coffee ?</h3></span>
						
					</div>
					<div class="about-accordian-content">
				    Here are the top producers of coffee in the world.
					<p>Brazil</p>
					<p>Vietnam</p>
					<p>Colombia</p>
					<p>Indonesia</p>
					<p>Ethiopia</p>
					<p>India</p>
					<p>Honduras</p>
					<p>Mexico</p>
					<p>Uganda</p>
					<p>Guatemala</p>
				
					</div>
				</li>
								<li class="about-li">
					<input type="checkbox" class="input2" checked>
					<div class="about-accordian-head">
						<span class="add-button">
							<img src="/website/images/demo/plus.png" class="plus" />
							<img src="/website/images/demo/minus.png" class="minus" />
						</span>
						<span class="heading-span"><h3>Why do grinders matter ?</h3></span>
						
					</div>
					<div class="about-accordian-content">
					Once ground coffee  should be consumed as fast as possible.This is because once the beans are grinded there is more surface area exposed to air and much of its flavor will be lost .Depending on how you are brewing coffee you will want to have a particular grind coffee .
					 
					</div>
				</li>
								<li class="about-li">
					<input type="checkbox" class="input2" checked>
					<div class="about-accordian-head">
						<span class="add-button">
														<img src="/website/images/demo/plus.png" class="plus" />
							<img src="/website/images/demo/minus.png" class="minus" />
						</span>
						<span class="heading-span"><h3>How important is water in preparation of coffee ?</h3></span>
						
					</div>
					<div class="about-accordian-content">
				Water makes up 99% of the brewed cup of coffee but often it is over looked.There are two things to remember about water 
				<p>1.Filtration </p>
				<p>2.Temperature</p>
				<p></p>
				<p><span>*</span>Filtration : If possible avoid tap water and use filtered water instead for brewing your coffee .<p/>
				<p><span>*</span>Temperature:It is recommended to heat water approximately to 195 to 205 degrees Fahrenheit( approx 93 Celsius).if the water is not heated ideal to this temperature essential oils will not be extracted .</p>
				

					</div>
				</li>
								<li class="about-li">
					<input type="checkbox" class="input2" checked>
					<div class="about-accordian-head">
						<span class="add-button">
														<img src="/website/images/demo/plus.png" class="plus" />
							<img src="/website/images/demo/minus.png" class="minus" />
						</span>
						<span class="heading-span"><h3>What are the different espresso based drinks ?</h3></span>
						
						</div>
					<div class="about-accordian-content">
					Americano,latte,mocha,cappuccino,espresso macchiato, latte macchiato,flat white,cortado,galao,breve,long black ,affagato,red eye etc.
					

					</div>
				</li>
								<li class="about-li">
					<input type="checkbox" class="input2" checked>
					<div class="about-accordian-head">
						<span class="add-button">
														<img src="/website/images/demo/plus.png" class="plus" />
							<img src="/website/images/demo/minus.png" class="minus" />
						</span>
						<span class="heading-span"><h3>What are the different methods of preparation of coffee?</h3></span>
						
					</div>
					<div class="about-accordian-content">
					The difffrent methods of preparation of coffee are:
					<p>French press</p>
					<p>Siphon pot</p>
					<p>Chemex</p>
					<p>Aeropress </p>
					<p>Mocha pot</p>
					
					</div>
				</li>
								<li class="about-li">
					<input type="checkbox" class="input2" checked>
					<div class="about-accordian-head">
						<span class="add-button">
														<img src="/website/images/demo/plus.png" class="plus" />
							<img src="/website/images/demo/minus.png" class="minus" />
						</span>
						<span class="heading-span"><h3>How to store coffee ??</h3></span>
						
					</div>
					<div class="about-accordian-content">
					Probably the best storage containers are made from glass or glazed  ceramic which have the added benefit of being  easily cleaned .If glass is used the container should be kept in dark location,the containers must be able to maintain air and moisture -proof sea
					</div>
				</li>
								<li class="about-li">
					<input type="checkbox" class="input2" checked>
					<div class="about-accordian-head">
						<span class="add-button">
														<img src="/website/images/demo/plus.png" class="plus" />
							<img src="/website/images/demo/minus.png" class="minus" />
						</span>
						<span class="heading-span"><h3>How much caffeine is there in decaf coffee ? </h3></span>
										</div>
					<div class="about-accordian-content">
					Decaf should range somewhere in the 2-4 milligrams of caffeine per cup range .
					</div>
				</li>
								<li class="about-li">
					<input type="checkbox" class="input2" checked>
					<div class="about-accordian-head">
						<span class="add-button">
														<img src="/website/images/demo/plus.png" class="plus" />
							<img src="/website/images/demo/minus.png" class="minus" />
						</span>
						<span class="heading-span"><h3>What is kopi luwak ? </h3></span>
						
					</div>
					<div class="about-accordian-content">
												
   					The only coffee of commerce today that is the product of an animals digestive tract is kopi luak or luwak from Sumatra ,Java and Sulawesi in Indonesia .

					</div>
				</li>
					
					

										</div>
				</div>
				</ul>
				</span>
				
		</div>
	</div>
</div>
	
	
	  
      </main>
	  </div>

<div class="wrapper row4">
  <footer id="footer" class="clear"> 
   
    <div class="one_quarter first">
      <h6 class="title">Company Details</h6>
      <address class="btmspace-15">
      #1, Bhadrappa Layout,1st Main,1st cross, <br>
	 Kodigehalli Main Road,<br>
	 Near Big Bazar<br>
     Bangalore-560094<br/>
	 India
      </address>
      <ul class="nospace">
        <li class="btmspace-10"><span class="fa fa-phone"></span>+91 (886) 122 1284</li>
		<li class="btmspace-10"><span class="fa fa-phone"></span>+91 (966) 373 0974</li>
        <li><span class="fa fa-envelope-o"></span> mail@berryco.info</li>
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="title">From The Blog</h6>
      <article>
        <h2 class="nospace font-x1"><a href="#">CUP OF JOY</a></h2>
        <time class="smallfont" datetime="2045-04-06">Monday, 23<sup>rd</sup> November 2015</time>
        <p>Someone has very rightly said…Coffee is nothing but a hug in a mug. A cup of coffee shared with a friend or a loved one is always happiness shared and time very well spent... </p>
      </article>
    </div>
    
	<div class="one_quarter">
      <h6 class="title">Our Belief</h6>
      <p>We believe that qualitity is the only undying method of building trust and retaining customer. </p>
      <p>When you change the was look at things, the things you look at will change-Wayne Dyer </p>
    </div>
	
	
    <div class="one_quarter">
      <h6 class="title">Our Service</h6>
      <p>We at BerryCo believe coffee is more then just a drink and would rediscover the value around your coffee. </p>
      <p>Drown your troubles in us and we develop strategic solutions and guidance on the path to success.</p>
    </div>
   
  </footer>
</div>


<div class="wrapper">
  <div id="socialblock" class="clear"> 
   
    <ul class="faico clear">
   <li><a class="faicon-facebook" href="https://www.facebook.com/BerryCo-506568626176433/"><i class="fa fa-facebook"></i></a></li>
      <li><a class="faicon-twitter" href="https://twitter.com/BerryCocoffee"><i class="fa fa-twitter"></i></a></li>
      <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
      <li><a class="faicon-google-plus" href="https://plus.google.com/u/0/104255358066214176181/about"><i class="fa fa-google-plus"></i></a></li>
      <li><a class="faicon-rss" href="https://www.addtoany.com/share?linkurl=www.berryco.info&amp;linkname="><i class="fa fa-share-alt"></i></a></li>
<!-- AddToAny BEGIN -->
<script type="text/javascript">
var a2a_config = a2a_config || {};
a2a_config.linkurl = "www.berryco.info";
a2a_config.onclick = 1;
</script>
<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->
    </ul>
   
  </div>
</div>

<div class="wrapper row5">
  <div id="copyright" class="clear"> 
   
    <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="#">BerryCo.info</a></p>
   
   
  </div>
</div>

<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a> 
<!-- JAVASCRIPTS --> 
<script src="/website/layout/scripts/jquery.min.js"></script> 
<script src="/website/layout/scripts/jquery.backtotop.js"></script> 
<script src="/website/layout/scripts/jquery.mobilemenu.js"></script> 
<!-- IE9 Placeholder Support --> 
<script src="/website/layout/scripts/jquery.placeholder.min.js"></script> 

<!-- / IE9 Placeholder Support -->
</body>
</html>