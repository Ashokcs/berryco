<?php

Route::get('/fb',array('as'=>'FBLogin','uses'=>'HomeController@loginWithFacebook'));

Route::get('/fb_signup',array('as'=>'FBSignup','uses'=>'HomeController@facebook_signup'));

Route::get('/gl',array('as'=>'GLLogin','uses'=>'HomeController@loginWithGoogle'));

Route::get('/gl_signup',array('as'=>'GLSignup','uses'=>'HomeController@google_signup'));


Route::get('/hello', function()
{
	return View::make('hello');
});

Route::get('/',array('as'=>'WebIndex','uses'=>'WebController@index'));

Route::get('/about',array('as'=>'WebAbout','uses'=>'WebController@about'));

Route::get('/service',array('as'=>'WebService','uses'=>'WebController@service'));

Route::get('/blog',array('as'=>'WebBlog','uses'=>'WebController@blog'));

Route::get('/faq',array('as'=>'WebFaq','uses'=>'WebController@faq'));

Route::get('weare',array('as'=>'WebWeare','uses'=>'WebController@weare'));

Route::get('qur',array('as'=>'WebQur','uses'=>'WebController@qur'));

Route::get('equipment',array('as'=>'WebEqu','uses'=>'WebController@equipment'));

Route::get('login',array('as'=>'WebLogin','uses'=>'WebController@login'));

Route::get('logout',array('as'=>'WebLogout','uses'=>'WebController@logout'));

Route::get('signup',array('as'=>'WebSignup','uses'=>'WebController@signup'));

Route::get('/save_signup',array('as'=>'WebSaveSignup','uses'=>'WebController@save_signup'));

Route::get('/checkout',array('as'=>'WebCheckout','uses'=>'WebController@checkout'));

Route::get('/remove_item/{id}',array('as'=>'WebRemoveItem','uses'=>'WebController@remove_item'));

Route::get('/full_remove_item/{id}',array('as'=>'WebFullRemoveItem','uses'=>'WebController@full_remove_item'));

Route::get('/put_session',array('as'=>'WebSession','uses'=>'WebController@put_session'));

Route::get('/add_cart',array('as'=>'WebAddCart','uses'=>'WebController@add_cart'));

Route::get('/buy_add_cart/{id}',array('as'=>'WebBuyAddCart','uses'=>'WebController@buy_add_cart'));

Route::post('/final_checkout',array('as'=>'WebFinalCheck','uses'=>'WebController@final_checkout'));

Route::post('/success_payment',array('as'=>'WebPaySuccess','uses'=>'PaymentController@success_payment'));

Route::post('/failure_payment',array('as'=>'WebPayFailure','uses'=>'PaymentController@failure_payment'));

Route::post('/payment',array('as'=>'WebPayment','uses'=>'PaymentController@payment'));



// Admin Panel 

Route::get('/admin/login',array('as'=>'AdminLogin','uses'=>'AdminController@login'));

Route::get('/admin/verify',array('as'=>'AdminVerify','uses'=>'AdminController@verify'));

Route::get('/admin/logout',array('as'=>'AdminLogout','uses'=>'AdminController@logout'));

Route::get('/admin/dashboard',array('as'=>'AdminDasboard','uses'=>'AdminController@dashboard'));

Route::get('/admin/add_brewers',array('as'=>'AdminAddBrewers','uses'=>'AdminController@add_brewers'));

Route::post('/admin/save_brewers',array('as'=>'AdminSaveBrewers','uses'=>'AdminController@save_brewers'));

Route::get('/admin/list_brewers',array('as'=>'AdminListBrewers','uses'=>'AdminController@list_brewers'));

Route::get('/admin/user_list_items/{id}',array('as'=>'AdminUserListItems','uses'=>'AdminController@user_list_items'));

Route::get('/admin/edit_brewer/{id}',array('as'=>'AdminEditBrewer','uses'=>'AdminController@edit_brewer'));

Route::post('/admin/update_brewer',array('as'=>'AdminUpdateBrewer','uses'=>'AdminController@update_brewer'));

Route::get('/admin/delete_brewer/{id}',array('as'=>'AdminDeleteBrewer','uses'=>'AdminController@delete_brewer'));

Route::get('/admin/add_items',array('as'=>'AdminAddItems','uses'=>'AdminController@add_item'));

Route::get('/admin/list_items',array('as'=>'AdminListItems','uses'=>'AdminController@list_items'));

Route::post('/admin/save_item',array('as'=>'AdminSaveItem','uses'=>'AdminController@save_item'));

Route::get('/admin/edit_item/{id}',array('as'=>'AdminEditItem','uses'=>'AdminController@edit_item'));

Route::post('/admin/update_item',array('as'=>'AdminUpdateItem','uses'=>'AdminController@update_item'));

Route::get('/admin/delete_item/{id}',array('as'=>'AdminDeleteItem','uses'=>'AdminController@delete_item'));

Route::get('/admin/list_users',array('as'=>'AdminListUsers','uses'=>'AdminController@list_users'));
